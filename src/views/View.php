<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 27.10.2016
 * Time: 10:47
 */

namespace amd_php_dev\yii2_components\views;

use amd_php_dev\yii2_components\models\MetaData;

class View extends \yii\web\View
{
    public $metaDataClass;

    /**
     * @var MetaData|null
     */
    protected $metaData;

    public function hasMetaData() {
        $model = $this->getMetaData();
        return (!$model->isNewRecord) && ($model->active == MetaData::ACTIVE_ACTIVE);
    }

    public function setMetaData($h1, $title, $description, $text, $active = null, $forceSave = false)
    {
        $model = $this->getMetaData();
        $model->h1 = $h1;
        $model->metaTitle = $title;
        $model->metaDescription = $description;
        $model->text = $text;

        if ($active !== null) {
            $model->active = $active;
        }

        if (
            ($model->active != MetaData::ACTIVE_BLOCKED && $model->active != MetaData::ACTIVE_ACTIVE)
            || $forceSave
        ) {
            $model->save();
        }
        $this->metaData = $model;
    }

    /**
     * @param $url
     * @return MetaData|array|null
     */
    public function getMetaData($url = null)
    {
        if (empty($url)) {
            $url = \yii::$app->request->url;
        }

        if (!empty($this->metaData) && $this->metaData->url == $url) {
            return $this->metaData;
        }

        $model = call_user_func([$this->metaDataClass, 'find'])->andWhere(['url' => $url])->one();

        if (empty($model) || (\yii::$app->request->hostName != \yii::$app->params['HOST'])) {
            $model = new $this->metaDataClass();
            $model->url = $url;
        }

        return $model;
    }
}