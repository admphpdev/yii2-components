<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 08.05.16
 * Time: 5:32
 */

namespace amd_php_dev\yii2_components\assets;


class BaseAsset extends Asset
{
    public $css = [
        'css/common.css',
        'plugins/dropzone/dropzone.css',
        'plugins/magnific-popup/magnific-popup.css',
        'plugins/datatables/DataTables-1.10.10/css/dataTables.bootstrap.min.css',
    ];
    
    public $js = [
        'js/lib.js',
        'plugins/dropzone/dropzone.js',
        'plugins/magnific-popup/jquery.magnific-popup.min.js',
        'plugins/datatables/datatables.min.js',
        'plugins/datatables/DataTables-1.10.10/js/dataTables.bootstrap.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}