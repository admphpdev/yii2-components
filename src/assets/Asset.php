<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 24.05.2016
 * Time: 12:32
 */

namespace amd_php_dev\yii2_components\assets;


class Asset extends \yii\web\AssetBundle
{
    public $baseUrl = '@web';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public function init()
    {
        $this->sourcePath = __DIR__ . '/../public';
        parent::init();
    }
}