<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 08.05.16
 * Time: 5:40
 */

namespace amd_php_dev\yii2_components\assets;


class AdminAsset extends Asset
{
    public $css = [
        'css/admin.css'
    ];
    public $js = [
        'js/admin.js'
    ];

    public $depends = [
        'amd_php_dev\yii2_components\assets\BaseAsset'
    ];
}