<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 08.05.16
 * Time: 5:41
 */

namespace amd_php_dev\yii2_components\assets;


class PublicAsset extends Asset
{
    public $css = [
        'css/site.css'
    ];
    public $js = [
    ];

    public $depends = [
        'amd_php_dev\yii2_components\assets\BaseAsset'
    ];
}