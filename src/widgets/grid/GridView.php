<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 23.06.2016
 * Time: 7:59
 */

namespace amd_php_dev\yii2_components\widgets\grid;


class GridView extends \yii\grid\GridView
{
    protected function initColumns()
    {
        parent::initColumns();

        foreach ($this->columns as $column) {
            if (!empty($column->attribute)) {
                $column->headerOptions['data-attribute'] = $column->attribute;
            }
        }
    }
}