<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 25.11.15
 * Time: 14:46
 */

namespace amd_php_dev\yii2_components\widgets\grid;

use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class UrlColumn extends DataColumn
{
    /**
     * @var mixed Параметры для ссылки, передаваемые в модель
     */
    public $itemUrlParams;

    /**
     * @param $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $value = $this->getDataCellValue($model, $key, $index);
        $url = empty($this->itemUrlParams) ? $model->getItemUrl() : $model->getItemUrl($this->itemUrlParams);
        if ($url) {
            return Html::a($value, $url);
        } else {
            return $value;
        }

    }
}