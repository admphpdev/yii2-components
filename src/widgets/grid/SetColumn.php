<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 25.11.15
 * Time: 9:32
 */

namespace amd_php_dev\yii2_components\widgets\grid;

use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use \amd_php_dev\yii2_components\widgets\form\SmartInput;

class SetColumn extends DataColumn
{
    public $inputData;
    public $formAction;
    public $valueToClass = [];
    /*
     * Устанавливать фильтр из модели
     */
    public $setFilter = false;

    public function init()
    {
        parent::init();

        $model = $this->grid->filterModel;

        if (empty($this->valueToClass) && !empty($model->getInputOptions($this->attribute)['valueToClass'])) {
            $this->valueToClass = $model->getInputOptions($this->attribute)['valueToClass'];
        }

        // Устанавливает фильтр из модели
        if (empty($this->filter) && $this->setFilter ) {
            if ($model instanceof \amd_php_dev\yii2_components\interfaces\SmartInputInterface && $filterData = $model->getInputData($this->attribute)) {
                $this->filter = $filterData;
            }
        }
    }

    /**
     * @param \amd_php_dev\yii2_components\models\Page $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $htmlOptions = [
            'class' => 'form-control',
            'data-ajax-action' => 'online-change',
            'data-form-action' => $this->_formAction($model)
        ];

        $value = $this->getDataCellValue($model, $key, $index);

        if (!empty($this->valueToClass) && !empty($this->valueToClass[$value])) {
            $htmlOptions['class'] .= ' ' . $this->valueToClass[$value];
        }

        $type = $model->getInputType($this->attribute);
        if ($type == \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_SELECT) {
            $type = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REGULAR_SELECT;
        }

        $smartInput = new SmartInput([
            'model' => $model,
            'attribute' => $this->attribute,
            'data' => $this->inputData,
            'type' => $type,
            'options' => [
                'htmlOptions' => $htmlOptions
            ]
        ]);

        return $smartInput->run();
    }

    protected function _formAction($model) {
        if (empty($this->formAction) && empty($this->formAction['route']))
            return false;

        $params = [$this->formAction['route']];
        if (!empty($this->formAction['params'])) {
            foreach ($this->formAction['params'] as $key => $value) {
                if ($value[0] == ':' && !empty($model->{substr($value, 1)})) {
                    $params[$key] = $model->{substr($value, 1)};
                } else {
                    $params[$key] = $value;
                }
            }
        }

        return Url::to($params);
    }
}