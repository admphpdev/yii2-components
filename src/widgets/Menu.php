<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 04.04.16
 * Time: 12:03
 */

namespace amd_php_dev\yii2_components\widgets;


use \yii\helpers\ArrayHelper;
use \kartik\nav\NavX as NavWidget;


class Menu extends \yii\base\Widget
{
    /**
     * Текущая секция
     * @var string
     */
    public $section = 'main';
    public $orientation = 'horizontal';
    public $options;
    public $items = [];
    protected static $_items;

    /**
     * Добавляет элементы меню в определённые секции
     *  [
     *      'section' => 'main',
     *      'items' => [
     *          массив элементов как для \yii\bootstrap\Nav
     *       ]
     *  ], ...
     *
     * Элементы из секции main выводятся в публичной части сайта
     * @param array $items
     * @return bool
     */
    public function addItems(array $items) {
        if (!is_array($items) && empty($items))
            return false;

        if (ArrayHelper::isAssociative($items) && !empty($items['section']) && !empty($items['section'])) {
            $this->_addItemsInSection($items['section'], $items['items']);
        } else {
            $section = 'main';
            $itemsIn = [];
            foreach ($items as $item) {
                if (is_array($item)) {
                    $this->addItems($item);
                } else {
                    $itemsIn[] = $item;
                }
            }

            $this->_addItemsInSection($section, $itemsIn);
        }
    }

    /**
     * Возвращает элементы меню из текущей секции
     */
    public function getItems() {
        if (!empty(static::$_items[$this->section])) {
            $items = static::$_items[$this->section];
        } else {
            $items = [];
        }

        return ArrayHelper::merge($items, $this->items);
    }

    /**
     * Добавляет массив элементов меню в определённую секцию
     * @param $section
     * @param $items
     */
    protected function _addItemsInSection($section, $items) {
        if (empty(static::$_items[$section])) {
            static::$_items[$section] = $items;
        } else {
            static::$_items[$section] = ArrayHelper::merge(static::$_items[$section], $items);
        }
    }

    /**
     * Реализует функционал синглтона (возвращает экземпляр класса)
     * @return static
     */
    public static function getInstance()
    {
        static $instance;
        if (empty($instance)) {
            $instance = new static();
        }
        return $instance;
    }

    public function run()
    {
        echo NavWidget::widget([
            'items' => $this->getItems(),
            'options' => $this->options,
        ]);
    }
}