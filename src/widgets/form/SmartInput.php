<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 02.12.15
 * Time: 18:05
 */

namespace amd_php_dev\yii2_components\widgets\form;


use yii\base\Exception;
//use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class SmartInput extends \yii\base\Widget
{
    const TYPE_TEXT             = 0;
    const TYPE_SELECT           = 1;
    const TYPE_CHECKBOX         = 2;
    const TYPE_NUMBER           = 3;
    const TYPE_TEXTAREA         = 4;
    const TYPE_RADIO            = 5;
    const TYPE_IMAGE_SINGLE     = 6;
    const TYPE_REDACTOR         = 7;
    const TYPE_FILEMANAGER      = 8;
    const TYPE_SMARTTABLE       = 9;
    const TYPE_GALLERY_IMAGE    = 10;
    const TYPE_GALLERY_FILE     = 11;
    const TYPE_GALLERY_VIDEO    = 12;
    const TYPE_REGULAR_SELECT   = 13;
    const TYPE_HIDDEN           = 14;
    const TYPE_TAGS             = 15;
    const TYPE_CATEGORIES       = 16;
    const TYPE_OPTIONS          = 17;
    const TYPE_READONLY         = 18;
    const TYPE_PASSWORD         = 19;
    const TYPE_CAPTCHA          = 20;

    /**
     * Расово верный костыль
     * Нужен для скармливания \yii\widgets\ActiveField
     * Иначе он отказывается работать без формы
     * @var \yii\widgets\ActiveForm
     */
    public static $staticform;

    /**
     * @var \amd_php_dev\yii2_components\interfaces\SmartInputInterface
     */
    public $model;
    public $type;
    public $data;
    public $label = false;
    public $attribute;
    public $form;

    /**
     * Поле для хранения индекса для сложных инпутов
     * @var int
     */
    public $index;
    /**
     * Опции поля ввода
     * @var array
     */
    public $options = [];

    /**
     * Проверка и загрузка данных
     */
    public function init()
    {
        // Модель обязательна
        if (!isset($this->model)) {
            throw new Exception('Не передана модель!');
        }

        // Аттрибут обязателен
        if (empty($this->attribute)) {
            throw new Exception('Не указан атрибут модели!');
        }

        // Необходимо получить тип
        if (empty($this->type) && !($this->model instanceof \amd_php_dev\yii2_components\interfaces\SmartInputInterface)) {
            throw new Exception('Не указан тип или неверный интерфейс модели!');
        } elseif (is_null($this->type)) {
            $this->type = $this->model->getInputType($this->attribute);
        }

        if ($this->model instanceof \amd_php_dev\yii2_components\interfaces\SmartInputInterface) {
            $this->options = ArrayHelper::merge(
                $this->model->getInputOptions($this->attribute),
                $this->options
            );

            if (empty($this->options['htmlOptions'])) {
                $this->options['htmlOptions'] = [];
            }
        }

        if (empty($this->data)) {
            $this->data = $this->model->getInputData($this->attribute);
        }

        if (empty(self::$staticform)) {
            self::$staticform = new \amd_php_dev\yii2_components\widgets\form\DummyActiveForm();
        }

        if (empty($this->form)) {
            $this->form = self::$staticform;
        }
    }

    /**
     * Выводит или возвращает input в зависимости от $return
     *
     * @param bool $return
     * @return mixed
     */
    public function run($return = true)
    {
        $input = $this->_getInput();

        if ($return) {
            return $input;
        }

        echo $input;
    }


    protected function _getInput()
    {
        $input = new SmartField();
        $input->form = $this->form;
        $input->model = $this->model;
        $input->attribute = $this->attribute;
        if ($this->label === false) {
            $input->label(false);
        }

        switch ($this->type) {
            case self::TYPE_TEXTAREA :
                $input->textarea($this->options['htmlOptions']);
                break;

            case self::TYPE_REGULAR_SELECT :
                $input->dropDownList($this->data, $this->options['htmlOptions']);
                break;

            case self::TYPE_CHECKBOX :
                if (is_array($this->data) && empty($this->data)) {
                    $input->checkboxList($this->data, $this->options['htmlOptions']);
                } else {
                    $input->checkbox($this->options['htmlOptions']);
                }
                break;

            case self::TYPE_NUMBER :
                $inputOptions = array_merge($this->options['htmlOptions'], ['type' => 'number']);
                $input->textInput($inputOptions);
                break;
            case self::TYPE_RADIO :
                if (is_array($this->data) && empty($this->data)) {
                    $input->radioList($this->data, $this->options['htmlOptions']);
                } else {
                    $input->radio($this->options['htmlOptions']); // WTF?
                }
                break;

            case self::TYPE_REDACTOR :
                $input->widget(\dosamigos\ckeditor\CKEditor::className(), [
                    'options' => [
                        'rows' => 20,
                    ],
                    'clientOptions' => [
                        'filebrowserBrowseUrl' => Url::to(['/main/elfinder/manager']),
                        'extraPlugins' =>
                            'stylesheetparser,print,save,preview,colorbutton,font,justify,embed,tableresize,tabletools',
                    ],
                    'preset' => 'full',
                ]);
                break;

            case self::TYPE_IMAGE_SINGLE :
                $input->singleImageInput();
                break;

            case self::TYPE_FILEMANAGER :
                $input->widget(\mihaildev\elfinder\InputFile::className(), [
                    'language'      => 'ru',
                    'controller'    => 'main/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                    'options'       => ['class' => 'form-control'],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple'      => false       // возможность выбора нескольких файлов
                ]);
                break;
            case self::TYPE_SMARTTABLE:
                $input->widget(
                    \amd_php_dev\yii2_components\widgets\form\SmartTable\SmartTable::className(),
                    ['dataManager' => $this->data]
                );
                break;
            case self::TYPE_GALLERY_VIDEO:
            case self::TYPE_GALLERY_IMAGE:
                $input->widget(
                    '\amd_php_dev\yii2_components\widgets\form\gallery\Gallery',
                    \yii\helpers\ArrayHelper::merge($this->options,['dataManager' => $this->data])
                );
                break;
            case self::TYPE_OPTIONS:
                $input->widget(
                    '\amd_php_dev\yii2_components\widgets\form\options\OptionsFieldSet',
                    \yii\helpers\ArrayHelper::merge(
                        $this->options,
                        [
                            'form' => $this->form,
                            'dataManager' => $this->data,
                        ]
                    )
                );
                break;
            case self::TYPE_SELECT:
                //$input->chosenSelect($this->data, $this->options);
                $input->select2($this->data, $this->options);
                break;
            case self::TYPE_TAGS:
                $input->tagsSelect2($this->data, $this->options);
                break;
            case self::TYPE_CATEGORIES:
                $input->categoriesSelect2($this->data, $this->options);
                break;
            case self::TYPE_HIDDEN:
                $input->label(false);
                $input->hiddenInput($this->options['htmlOptions']);
                break;
            case self::TYPE_READONLY:
                $this->options['htmlOptions']['readOnly'] = true;
                $input->textInput($this->options['htmlOptions']);
                break;
            case self::TYPE_PASSWORD:
                $input->passwordInput($this->options['htmlOptions']);
                break;
            case self::TYPE_CAPTCHA:
                $input->widget(Captcha::className(), $this->options['htmlOptions']);
                break;
            default :
                $input->textInput($this->options['htmlOptions']);

        }
        return $input;
    }

}