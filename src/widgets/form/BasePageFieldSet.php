<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 21.12.15
 * Time: 15:13
 */

namespace amd_php_dev\yii2_components\widgets\form;


class BasePageFieldSet extends \yii\base\Widget
{
    public $form;
    public $model;

    public $fields = [];

    public function init()
    {
        parent::init();

        if (empty($this->form)) {
            $this->form = new \yii\widgets\ActiveForm([
                'options' => ['enctype'=>'multipart/form-data']
            ]);
        }

        $this->fields['priority'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'priority',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['active'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'active',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['name_small'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'name_small',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['name'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'name',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['url'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'url',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['meta_title'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'meta_title',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['meta_keywords'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'meta_keywords',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['meta_description'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'meta_description',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['text_small'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'text_small',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['text_full'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'text_full',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['image_small'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'image_small',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['image_full'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'image_full',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['links'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'links',
            'label'     => true,
            'form'      => $this->form,
        ]);

        $this->fields['snipets'] = SmartInput::widget([
            'model'     => $this->model,
            'attribute' => 'snipets',
            'label'     => true,
            'form'      => $this->form,
        ]);
    }

    /**
     * Render fields
     */
    public function run()
    {
        foreach($this->fields as $fieldName => $field) {
            echo $field;
        }
    }
}