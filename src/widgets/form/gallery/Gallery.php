<?php
/**
 * User: lagprophet
 * Date: 22.12.15
 * Time: 16:33
 */

namespace amd_php_dev\yii2_components\widgets\form\gallery;



class Gallery extends \yii\base\Widget
{
    const TYPE_IMAGE = 0;
    const TYPE_FILE  = 1;
    const TYPE_VIDEO = 2;

    /**
     * Модель для которой галерея
     * @var \amd_php_dev\yii2_components\models\SmartRecord
     */
    public $model;
    /**
     * Атрибут из модели для которой галерея
     * @var string
     */
    public $attribute;
    /**
     * Тип галереи
     * @var string
     */
    public $type;
    /**
     * Маршрут к действию добавления нового элемента в галерею
     *
     * Экшн должен возвращать HTML-кот нового элемента галереи
     * @var array
     */
    public $action;
    /**
     * @var \amd_php_dev\yii2_components\behaviors\GalleryManager
     */
    public $dataManager;

    public $htmlOptions;

    public $buttonTemplate;
//    protected $viewsPath = '@app/components/widgets/form/gallery/views/';
    protected $viewsPath = 'views/';
    protected $itemModelContentAttribute;

    public function init()
    {
        parent::init();

        $this->type = $this->dataManager->type;
        if ($this->type == \amd_php_dev\yii2_components\behaviors\GalleryManager::TYPE_VIDEO) {
            $this->buttonTemplate = __DIR__ . '/' . $this->viewsPath . 'gallery/_button-video.php';
        } else {
            $this->buttonTemplate = __DIR__ . '/' . $this->viewsPath . 'gallery/_button-file.php';
        }
    }

    public function run()
    {
        $this->view->registerJs(
            $this->renderFile(__DIR__ . '/' . $this->viewsPath . 'gallery/script.php'),
            \yii\web\View::POS_END,
            'widget-gallery-js'
        );

        return $this->renderFile(
            __DIR__ . '/' . $this->viewsPath . 'gallery/layout.php',
            [
                'widget' => $this,
                'dataManager' => $this->dataManager,
            ]
        );
    }
}