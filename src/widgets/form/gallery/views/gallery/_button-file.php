<?php
/**
 * @var \amd_php_dev\yii2_components\widgets\form\gallery\Gallery $widget
 */
?>


<div id="fileupload-<?= $widget->id ?>" class="row">

    <div class="col-lg-3">
        <!-- The fileinput-button span is used to style the file input field as button -->
        <div class="btn btn-primary fileinput-button dz-clickable"
            name="<?= $formName; ?>"
            data-plugin="ajax-upload"
            data-type="<?= $widget->type; ?>"
            data-url="<?= \yii\helpers\Url::to($widget->action); ?>"
            multiple
        >
            <i class="glyphicon glyphicon-plus"></i>
            <span>Добавить файлы...</span>
        </div>
    </div>

    <div class="col-lg-9">
        <!-- The global file processing state -->
        <span class="fileupload-process">
          <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="opacity: 0;">
              <div class="progress-bar progress-bar-success" style="width: 100%;" data-dz-uploadprogress=""></div>
          </div>
        </span>
    </div>

</div>