<?php
/**
 * @var \amd_php_dev\yii2_components\behaviors\GalleryManager $dataManager
 * @var \amd_php_dev\yii2_components\widgets\form\gallery\Gallery $widget
 * @var int     $type            Тип галереи
 * @var string  $buttonTemplate  Путь к view батона
 * @var string  $modelFormName
 * @var array   $action          Маршрут к загрузчику элементов галереи @see Gallery
 */
?>
<div class="panel-group gallery-container" id="gallery-container-<?= $widget->id; ?>" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="gallery-heading-<?= $widget->id; ?>">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#gallery-container-<?= $widget->id; ?>"
                   href="#gallery-panel-<?= $widget->id; ?>" aria-expanded="true" aria-controls="gallery-panel-<?= $widget->id; ?>"
                >
                    <?= $dataManager->getLabel(); ?>
                </a>
            </h4>
        </div>
        <div id="gallery-panel-<?= $widget->id; ?>" class="gallery-panel panel-collapse collapse in" role="tabpanel" aria-labelledby="gallery-heading-<?= $widget->id; ?>">
            <div class="panel-body">
                <div class="col-md-12 gallery-items__container">
                    <?php
                        $items = $dataManager->getItems();
                        if (!empty($items)) {
                            foreach ($items as $item) {
                                echo \amd_php_dev\yii2_components\widgets\form\gallery\GalleryItem::widget([
                                    'model'         => $item,
                                    'type'          => $dataManager->type,
                                ]);
                            }
                        }
                    ?>
                </div>
                <div class="col-md-12 gallery-items__button">
                    <hr>
                    <?= $this->renderFile(
                        $widget->buttonTemplate,
                        [
                            'widget' => $widget,
                            'formName' => $dataManager->galleryItemFormName
                        ]
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</div>