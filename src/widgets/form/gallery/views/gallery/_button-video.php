<button
    name="<?= $formName; ?>"
    data-url="<?= \yii\helpers\Url::to($widget->action); ?>"
    data-type="<?= $widget->type; ?>"
    class="btn btn-primary add" data-plugin="video-upload">
    <i class="glyphicon glyphicon-plus"></i>
    <span>Добавить</span>
</button>
