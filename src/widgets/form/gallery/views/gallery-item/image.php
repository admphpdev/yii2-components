<?php
/**
 * Превью для элемента галереи
 */
?>
<div class="gallery-item-form-block">
    <!-- Изображение -->
    <div class="modal fade" id="image-gallery-item-modal-<?= $model->id ?>" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <div class="modal-title">Редактировать элемент</div>
                </div>

                <div class="modal-body">
                    <img class="img-responsive center-block" src="" alt="">
                    <?php foreach($widget->attributes as $attribute) : ?>
                        <?php echo \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
                            'model'     => $model,
                            'attribute' => $attribute,
                            'label'     => true,
                        ]);
                        ?>
                    <?php endforeach;?>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>

            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-4" id="image-gallery-item-<?= $model->id ?>">
        <div class="thumbnail">

            <img src="<?= $model->content; ?>" alt="...">

            <div class="caption">
                <div class="row">
                    <div class="col-md-4">
                        <a
                            href="#image-gallery-item-modal-<?= $model->id ?>"
                            class="btn btn-warning" data-toggle="modal"
                        >
                            Редактировать
                        </a>
                    </div>
                    <div class="col-md-4 col-md-offset-3" data-delete="#image-gallery-item-modal-<?= $model->id ?>, #gallery-item-<?= $model->id ?>">
                        <span class="btn btn-danger delete">Удалировать</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>