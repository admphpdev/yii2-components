<?php
/**
 * User: lagprophet
 * Date: 22.12.15
 * Time: 16:39
 */

namespace amd_php_dev\yii2_components\widgets\form\gallery;


class GalleryItem extends \yii\base\Widget
{

    public $model;
    public $attributes = [];
    public $type;
//    protected $viewsPath = '@app/components/widgets/form/gallery/views/gallery-item/';
    protected $viewsPath = 'views/gallery-item/';

    public function init()
    {
        if (empty($this->attributes)) {
            $this->attributes = $this->model->activeAttributes();
        }
    }

    public function run()
    {
        return $this->renderFile(
            __DIR__ . '/' . $this->viewsPath . $this->type . '.php',
            [
                'widget' => $this,
                'model' => $this->model,
            ]
        );
    }
}