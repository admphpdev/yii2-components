<?php
/**
 * User: lagprophet
 * Date: 14.12.15
 * Time: 16:37
 */

namespace amd_php_dev\yii2_components\widgets\form\SmartTable;


class SmartTable extends \yii\base\Widget
{
    public $model;
    public $attribute;

    /**
     * @var \amd_php_dev\yii2_components\behaviors\CustomObjectsFieldBehavior
     */
    public $dataManager;

    /**
     * @inheritdoc
     */
    public function run($return = false)
    {

        return $this->renderFile(__DIR__ . '/views/table.php',
            [
                /**
                 * Список полей объекта
                 * @var array
                 */
                'fieldsList' => $this->dataManager->objectAttributes,
                /**
                 * Массив объектов
                 * @var array
                 */
                'objects'   => $this->dataManager->getObjects(),
                'model'     => $this->model,
                'attribute' => $this->attribute,
                'newObject' => $this->dataManager->getObject(),
            ]
        );
    }
}