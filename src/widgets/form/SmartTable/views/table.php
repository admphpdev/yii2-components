<?php
/**
 * User: lagprophet
 * Date: 14.12.15
 * Time: 16:39
 */
use \yii\helpers\Html;
$inputId = strtolower($model->formName()) . '-' . $attribute;
?>

<table class="table table-striped table-bordered" id="<?= $inputId ?>" data-plugin="datatables">
    <thead>
        <tr>
            <th>#</th>
            <?php foreach ($fieldsList as $fieldAlias => $fieldName) :?>
                <th><?= $fieldName ?></th>
            <?php endforeach; ?>
            <th>Удалить</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($objects)) :?>
            <?php foreach ($objects as $index => $object) :?>
                <tr>
                    <td><?= $index + 1 ?></td>

                    <?php foreach ($fieldsList as $fieldAlias => $fieldName) :?>
                        <td>
                            <?=
                                \amd_php_dev\yii2_components\widgets\form\SmartInput::widget(
                                    [
                                        'model'     => $object,
                                        'attribute' => $fieldAlias,
                                        'label'     => false,
                                        'index'     => $index + 1,
                                    ]
                                );
                            ?>
                        </td>
                    <?php endforeach; ?>

                    <td><button class="btn btn-danger  deleteRow">Удалить</button></td>
                </tr>
            <?php endforeach; ?>
        <?php endif ?>
    </tbody>
    <tfoot>
        <td>#</td>

        <?php foreach ($fieldsList as $fieldAlias => $fieldName) :?>
            <td>
                <?=
                \amd_php_dev\yii2_components\widgets\form\SmartInput::widget(
                    [
                        'model'     => $newObject,
                        'attribute' => $fieldAlias,
                        'label'     => false,
                    ]
                );
                ?>
            </td>
        <?php endforeach; ?>

        <td><a href="#" class="btn btn-primary addNewRow">Добавить</a></td>
    </tfoot>
</table>
