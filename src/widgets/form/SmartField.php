<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 09.12.15
 * Time: 19:37
 */

namespace amd_php_dev\yii2_components\widgets\form;
use kartik\widgets\Select2;
use \yii\helpers\Html;

class SmartField extends \yii\widgets\ActiveField
{
    public function singleImageInput($options = [])
    {
        if (!empty($options['writeScript'])) {
            $this->script($options['writeScript']);
        }

        // https://github.com/yiisoft/yii2/pull/795
        if ($this->inputOptions !== ['class' => 'form-control']) {
            $options = array_merge($this->inputOptions, $options);
        }
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = '';

        if (!empty($this->model->{$this->attribute})) {
            $this->parts['{input}'] .= Html::tag(
                'div',
                Html::tag(
                    'div',
                    Html::a(
                        Html::img($this->model->{$this->attribute}, [
                            'class' => 'img-thumbnail',
                            'alt'   => $this->attribute,
                            'title' => $this->attribute,
                        ]),
                        $this->model->{$this->attribute},
                        ['class' => 'thumbnail thumbnail-single']
                    ),
                    ['class' => 'col-xs-6 col-md-3']
                ),
                ['class' => 'row']
            );
        }

        $this->parts['{input}'] .= Html::activeFileInput($this->model, $this->attribute, $options);

        return $this;
    }

    public function chosenSelect($items, $options = [])
    {
        if (!empty($options['writeScript'])) {
            $this->script($options['writeScript']);
        }
        $options['htmlOptions']['data-chosen'] = true;
        if (!empty($options['multiple'])) {
            $options['htmlOptions']['multiple'] = $options['multiple'];
        }
        $this->parts['{input}'] = Html::activeDropDownList($this->model, $this->attribute, $items, $options['htmlOptions']);
    }

    public function select2($items, $options = [])
    {
        if (!empty($options['writeScript'])) {
            $this->script($options['writeScript']);
        }
        $name = isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute);

        $options['htmlOptions']['placeholder'] = $name;
        $selected = Html::getAttributeValue($this->model, $this->attribute);
        if (!is_array($selected)) {
            $selected = [$selected];
        }

        $this->parts['{input}'] = Select2::widget([
            'id' => md5(time() * rand(1, 100)),
            'name' => $name,
            'theme' => Select2::THEME_BOOTSTRAP,
            'language' => \Yii::$app->language,
            'value' => $selected,
            'data' => $items,
            'options' => array_merge(
                $options['htmlOptions'],
                ['placeholder' => $this->model->getAttributeLabel($this->attribute)]
            ),
            'pluginOptions' => [
//                'allowClear' => true
            ],
        ]);
    }

    public function tagsSelect2($items, $options = [])
    {
        if (!empty($options['writeScript'])) {
            $this->script($options['writeScript']);
        }
        $name = isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute);

        $options['htmlOptions']['placeholder'] = $name;
        $selected = Html::getAttributeValue($this->model, $this->attribute);
        if (!is_array($selected)) {
            $selected = [$selected];
        }

        $this->parts['{input}'] = Select2::widget([
            'id' => md5(time() * rand(1, 100)),
            'name' => $name,
            'theme' => Select2::THEME_BOOTSTRAP,
            'language' => \Yii::$app->language,
            'value' => $selected,
            'data' => $items,
            'options' => array_merge(
                $options['htmlOptions'],
                [
                    'placeholder' => $this->model->getAttributeLabel($this->attribute),
                    'multiple' => true
                ]
            ),
            'pluginOptions' => [
                'tags' => true,
            ],
        ]);
    }

    public function categoriesSelect2($items, $options = [])
    {
        if (!empty($options['writeScript'])) {
            $this->script($options['writeScript']);
        }
        $name = isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute);

        $options['htmlOptions']['placeholder'] = $name;
        $selected = Html::getAttributeValue($this->model, $this->attribute);
        if (!is_array($selected)) {
            $selected = [$selected];
        }

        $this->parts['{input}'] = Select2::widget([
            'id' => md5(time() * rand(1, 100)),
            'name' => $name,
            'theme' => Select2::THEME_BOOTSTRAP,
            'language' => \Yii::$app->language,
            'value' => $selected,
            'data' => $items,
            'options' => array_merge(
                $options['htmlOptions'],
                [
                    'placeholder' => $this->model->getAttributeLabel($this->attribute),
                    'multiple' => true
                ]
            ),
        ]);
    }

    public function script($code)
    {
        if (!empty($code)) {
            \yii::$app->view->registerJs($code, \yii\web\View::POS_END);
        }

        return $this;
    }
}