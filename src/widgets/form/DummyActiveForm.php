<?php
/**
 * User: lagprophet
 * Date: 28.12.15
 * Time: 15:23
 */

namespace amd_php_dev\yii2_components\widgets\form;


use yii\widgets\ActiveForm;

/**
 * Class DummyActiveForm
 * Форма, которая не выводит себя где не надо, в отличии от пса-родителя
 * @package amd_php_dev\yii2_components\widgets\form
 */
class DummyActiveForm extends ActiveForm
{
    public function init()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Runs the widget.
     * This registers the necessary javascript code and renders the form close tag.
     * @throws InvalidCallException if `beginField()` and `endField()` calls are not matching
     */
    public function run()
    {
        if (!empty($this->_fields)) {
            throw new \yii\base\InvalidCallException('Each beginField() should have a matching endField() call.');
        }

        if ($this->enableClientScript) {
            $id = $this->options['id'];
            $options = \yii\helpers\Json::htmlEncode($this->getClientOptions());
            $attributes = \yii\helpers\Json::htmlEncode($this->attributes);
            $view = $this->getView();
            \yii\widgets\ActiveFormAsset::register($view);
            $view->registerJs("jQuery('#$id').yiiActiveForm($attributes, $options);");
        }
    }
}