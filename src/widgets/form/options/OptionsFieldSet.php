<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 28.04.16
 * Time: 12:45
 */

namespace amd_php_dev\yii2_components\widgets\form\options;


class OptionsFieldSet extends \yii\base\Widget
{
    /**
     * Модель
     * @var \amd_php_dev\yii2_components\models\SmartRecord
     */
    public $model;

    public $form;

    /**
     * Атрибут из модели
     * @var string
     */
    public $attribute;

    public $dataManager;

    public $htmlOptions;

    public $viewPath;

    public function init()
    {
        if (empty($this->viewPath)) {
            $this->viewPath = __DIR__ . '/views/';
        }
    }

    public function run()
    {
        if (!empty($this->dataManager->getOptions())) {
            return $this->renderFile(
                $this->viewPath . 'layout.php',
                [
                    'widget' => $this,
                    'dataManager' => $this->dataManager,
                ]
            );
        }
    }
}