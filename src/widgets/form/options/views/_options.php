<?php
/**
 * @var $dataManager \amd_php_dev\yii2_components\behaviors\OptionBehavior
 * @var $widget \amd_php_dev\yii2_components\widgets\form\options\OptionsFieldSet
 * @var $group \amd_php_dev\yii2_components\models\OptionGroup|null
 * @var $options \amd_php_dev\yii2_components\models\Option[]
 */
?>
<div class="b-options-list<?= !empty($group) ? ' b-groups-list__options' : ''; ?>">
    <?php foreach ($options as $option) :
        /**
         * @var $formModel \amd_php_dev\yii2_components\models\OptionValueForm
         */
        $formModel = new $dataManager->valueFormModelClass;
        $value = !empty($dataManager->getValue()[$option->id]) ? $dataManager->getValue()[$option->id] : null;
        $errors = !empty($dataManager->errors[$option->id]) ? $dataManager->errors[$option->id] : null;
        $formModel->setData(
            $dataManager->owner,
            $option,
            $dataManager->setableAttribute,
            $value,
            $errors
        );
        ?>

        <div class="b-options-list__item b-option-item">
            <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
                'form'      => $widget->form,
                'model'     => $formModel,
                'attribute' => 'value',
                'label'     => true,
            ]); ?>
        </div>

    <?php endforeach; ?>
</div>