<?php
/**
 * @var $dataManager \amd_php_dev\yii2_components\behaviors\OptionBehavior
 * @var $widget \amd_php_dev\yii2_components\widgets\form\options\OptionsFieldSet
 */
?>

<div class="b-options-field-set">
    <?php if (!empty($dataManager->getOptionGroups())) : ?>
        <?= $this->renderFile(
                $widget->viewPath . '_groups.php',
                [
                    'widget' => $widget,
                    'dataManager' => $dataManager
                ]
            );
        ?>
    <?php else: ?>
        <?= $this->renderFile(
            $widget->viewPath . '_options.php',
            [
                'widget' => $widget,
                'dataManager' => $dataManager,
                'options' => $dataManager->getOptions()
            ]
        );
        ?>
    <?php endif; ?>
</div>
