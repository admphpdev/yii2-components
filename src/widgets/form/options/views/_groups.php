<?php
/**
 * @var $dataManager \amd_php_dev\yii2_components\behaviors\OptionBehavior
 * @var $widget \amd_php_dev\yii2_components\widgets\form\options\OptionsFieldSet
 */
?>

<div class="b-options-field-set__groups-list b-groups-list">
    <?php
        $groups = $dataManager->getOptionGroups();
        foreach ($groups as $group) :
            $options = $dataManager->getOptionsBy('id_group', $group->id);
    ?>
        <?php if (!empty($options)) : ?>
            <div class="b-groups-list__title"><?= $group->name; ?></div>
            <div class="b-groups-list__options-list">
                <?= $this->renderFile(
                        $widget->viewPath . '_options.php',
                        [
                            'widget' => $widget,
                            'dataManager' => $dataManager,
                            'options' => $options,
                            'group' => $group
                        ]
                    );
                ?>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
