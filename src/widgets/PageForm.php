<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 24.11.15
 * Time: 9:52
 */

namespace amd_php_dev\yii2_components\widgets;

use \yii\helpers\ArrayHelper;
use app\models\Page as PageModel;
use vova07\imperavi\Widget as ImperaviRedactor;
use yii\helpers\Url;


class PageForm extends \yii\widgets\ActiveForm
{
    public $pageModel;

    protected $_fields = [];

    protected static $_standartConfig = [
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ];

    public static function begin($config = [])
    {
        /**
         * @var $widget PageForm
         */
        $widget = parent::begin(ArrayHelper::merge(static::$_standartConfig, $config));
        $widget->_fields = $widget->getStandartFields();

        if (!empty($config['pageModel']) && $config['pageModel'] instanceof PageModel) {
            $widget->pageModel = $config['pageModel'];
        }

        return $widget;
    }

    /**
     * Задаёт модель для которой будет создаваться форма
     * @param PageModel $pageModel
     */
    public function setPageModel(PageModel $pageModel) {
        $this->pageModel = $pageModel;
    }

    /**
     * Возвращает шаблон превьюшки картинки
     * @param $imageAttr Аттрибут модели, откуда брать ссылку
     * @return string
     */
    public function getImageFieldTemplate($imageAttr) {
        return "{label}\n<img src=\"{$this->pageModel->getImageFileUrl($imageAttr)}\" class=\"img-thumbnail display-block\">\n{input}\n{hint}\n{error}";
    }

    /**
     * Удаляет поле из набора полей
     * @param $fieldName
     */
    public function excludeField($fieldName) {
        if (!empty($this->_fields[$fieldName])){
            unset($this->_fields[$fieldName]);
        }
    }

    /**
     * Возвращает массив стандартных полей
     * @return array
     * @throws InvalidCallException
     */
    public function getStandartFields() {
        if (empty($this->pageModel))
            throw new InvalidCallException(Yii::t("base/widgets/pageForm", 'ACTIVE_BLOCKED'));

        $this->pageModel->priority = empty($this->pageModel->priority) ? 500 : $this->pageModel->priority;
        $this->pageModel->active = ($this->pageModel->isNewRecord) ? PageModel::ACTIVE_SKETCH : $this->pageModel->active;

        return [
            'name'              => $this->field($this->pageModel, 'name')->textInput(['maxlength' => true]),
            'url'               => $this->field($this->pageModel, 'url')->textInput([
                                    'maxlength' => true,
                                    'data-translit' => "[name=\"{$this->pageModel->formName()}[name]\"]"
                                ]),
            'meta_title'        => $this->field($this->pageModel, 'meta_title')->textInput(['maxlength' => true]),
            'meta_keywords'     => $this->field($this->pageModel, 'meta_keywords')->textInput(['maxlength' => true]),
            'meta_description'  => $this->field($this->pageModel, 'meta_description')->textInput(['maxlength' => true]),
            'text_small'        => $this->field($this->pageModel, 'text_small')->textarea(['rows' => 6]),
            'text_full'         => $this->field($this->pageModel, 'text_full')->widget(ImperaviRedactor::className(), [
                                        'settings' => [
                                            'lang' => preg_replace('/(\w+)-\w+/', '$1', \Yii::$app->language),
                                            'minHeight' => 600,
                                            'imageUpload' => Url::to(\Yii::$app->params['redactorUploadUrl']),
                                            'plugins' => [
                                                'clips',
                                                'fullscreen',
                                                'table',
                                                'video'
                                            ]
                                        ]
                                    ]),
            'image_small'       => $this->field($this->pageModel, 'image_small',
                                                ['template' => $this->getImageFieldTemplate('image_small')])->fileInput(),
            'image_full'        => $this->field($this->pageModel, 'image_full',
                                                ['template' => $this->getImageFieldTemplate('image_full')])->fileInput(),
            'priority'          => $this->field($this->pageModel, 'priority')->textInput(),
            'active'            => $this->field($this->pageModel, 'active')->dropdownList(PageModel::getActiveArray())
        ];
    }

    /**
     * Генерирует HTML полей формы
     */
    public function renderFields() {
        foreach ($this->_fields as $field) {
            if (!empty($field)) {
                echo $field;
            }
        }
    }
}