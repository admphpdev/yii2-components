<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 07.12.15
 * Time: 14:51
 */

namespace amd_php_dev\yii2_components\behaviors\nestedsets;


use yii\base\Behavior;
use yii\db\Expression;
/**
 * NestedSetsQueryBehavior
 *
 * @property \yii\db\ActiveQuery $owner
 *
 * @author Alexander Kochetov <creocoder@gmail.com>
 */
class NestedSetsQueryBehavior extends Behavior
{
    /**
     * Gets the root nodes.
     * @return \yii\db\ActiveQuery the owner
     */
    public function roots()
    {
        $model = new $this->owner->modelClass();
        $this->owner
            ->andWhere([$model->leftAttribute => 1])
            ->addOrderBy([$model->primaryKey()[0] => SORT_ASC]);
        return $this->owner;
    }

    public function noRoot()
    {
        $model = new $this->owner->modelClass();
        $this->owner
            ->andWhere(['<>', $model->leftAttribute, 1]);
        return $this->owner;
    }

    /**
     * Gets the leaf nodes.
     * @return \yii\db\ActiveQuery the owner
     */
    public function leaves()
    {
        $model = new $this->owner->modelClass();
        $db = $model->getDb();
        $columns = [$model->leftAttribute => SORT_ASC];
        if ($model->treeAttribute !== false) {
            $columns = [$model->treeAttribute => SORT_ASC] + $columns;
        }
        $this->owner
            ->andWhere([$model->rightAttribute => new Expression($db->quoteColumnName($model->leftAttribute) . '+ 1')])
            ->addOrderBy($columns);
        return $this->owner;
    }

    /**
     * Набор условий для выбоки в хлебные крошки
     * @return $this
     */
    public function toBreadcrumbs()
    {
        return $this->owner->active()->addOrderBy('depth ASC');
    }

    /**
     * Набор условий для выбоки в меню
     * @return $this
     */
    public function toMenu()
    {
        return $this->owner->active()->orderBy('priority DESC, depth ASC');
    }

    public function toInput()
    {
        return $this->owner->active()->orderBy('priority DESC, depth ASC');
    }
}