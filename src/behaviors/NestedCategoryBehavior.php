<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 14.12.15
 * Time: 11:20
 */

namespace amd_php_dev\yii2_components\behaviors;

/**
 * Class NestedCategoryBehavior
 * @package amd_php_dev\yii2_components\behaviors
 */
class NestedCategoryBehavior extends \amd_php_dev\yii2_components\behaviors\nestedsets\NestedSetsBehavior
{
    /**
     * @var Хранит результат работы getMenuItems
     * @see \amd_php_dev\yii2_components\behaviors\NestedCategoryBehavior::getMenuItems()
     */
    protected $menuItems;

    /**
     * @var Хранит результат работы getBreadcrumbsItems
     * @see \amd_php_dev\yii2_components\behaviors\NestedCategoryBehavior::getBreadcrumbsItems()
     */
    protected $breadcrumbsItems;

    public function events()
    {
        return \yii\helpers\ArrayHelper::merge(parent::events(),[
            \yii\db\ActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
        ]);
    }

    /**
     * Возвращает массив для использования в \yii\widgets\Menu
     * @param \amd_php_dev\yii2_components\models\Page $item
     * @param bool $rebuild Если true, то массив будет перестроен
     * @return array
     */
    public function getMenuItems($item = null, $rebuild = false)
    {
        if (!empty($this->menuItems) && !$rebuild && $item == null) {
            return $this->menuItems;
        }

        if ($item == null) {
            $thisMain = true;
            $item = $this->owner;
        }

        $result = [
            'label' => $item->name,
            'url' => $item->getItemUrl()
        ];

        $children = $item->children(1)->toMenu()->all();
        if (!empty($children) && count($children)) {
            $result['items'] = [];

            foreach($children as $child) {
                $result['items'][] = $this->getMenuItems($child);
            }
        }

        if (!empty($thisMain)) {
            return $this->menuItems = [$result];
        } else {
            return $result;
        }
    }

    /**
     * Возвращает массив для использования в \yii\widgets\Menu
     * @param \amd_php_dev\yii2_components\models\Page $item
     * @param bool $rebuild Если true, то массив будет перестроен
     * @return []
     */
    public function getBreadcrumbsItems($thisLink = false, $rebuild = false)
    {
        if (!empty($this->breadcrumbsItems) && !$rebuild) {
            return $this->breadcrumbsItems;
        }

        $result = [];

        $parents = $this->owner->parents()->toBreadcrumbs()->all();

        foreach ($parents as $parent) {
            $result[] = [
                'label' => $parent->name,
                'url' => $parent->getItemUrl()
            ];
        }

        $thisItem = [
            'label' => $this->owner->name,
        ];

        if ($thisLink) {
            $thisItem['url'] = $this->owner->getItemUrl();
        }

        $result[] = $thisItem;

        return $this->breadcrumbsItems =  $result;
    }

    public function afterValidate()
    {
        if (
            $this->owner->getIsNewRecord() ||
            $this->owner->getOldAttribute('id_parent') != $this->owner->id_parent
        ) {
            if (empty($this->owner->id_parent)) {
                $this->operation = self::OPERATION_MAKE_ROOT;
            } else {
                $this->operation = self::OPERATION_APPEND_TO;
                $this->node = $this->owner->find()->where(['id' => $this->owner->id_parent])->toInput()->one();
            }
        }

        return true;
    }

    public function getParentInputData()
    {
        $exclude = [];

        if (!$this->owner->getIsNewRecord()) {

            $childrenIds = $this->children()->toInput()->asArray()->select(['id'])->all();
            $exclude = array_map(
                function ($e) {
                    return $e['id'];
                },
                $childrenIds
            );

            $exclude[] = $this->owner->id;
        }

        $query = $this->owner->find();

        if (!$this->owner->getIsNewRecord()) {
            $query->where("id NOT IN (" . \amd_php_dev\yii2_components\helpers\DbHelper::quoteArray($exclude) . ")");
        }

        $items = $query->toInput()->asArray()->select(['id', 'name'])->all();

        return \yii\helpers\ArrayHelper::merge(['0' => 'Без родителя'], \yii\helpers\ArrayHelper::map($items, 'id' , 'name'));
    }
}