<?php
/**
 * User: lagprophet
 * Date: 22.12.15
 * Time: 17:55
 */

namespace amd_php_dev\yii2_components\behaviors;
use Guzzle\Http\Exception\BadResponseException;

/**
 *
 * Class GalleryManager
 *
 * Менеджер галерей
 * Лец ми спик фром май харт ин инглиш:
 * "фром Толстоквашино виз лав!".
 * ~~~
 * public function behaviors()
 * {
 *  return [
 *      ...
 *      'imageGallery' => [
 *          'class' => '\amd_php_dev\yii2_components\behaviors\GalleryManager',
 *          // 'relationType' => 'hasMany',
 *          'relationModelName' => 'название модели элементов галереи',
 *          // 'relationKeys' => ['id_item' => 'id'],
 *          // 'relationScopes' => [
 *              'active',
 *          ],
 *          'galleryAttribute' => 'ImageGallery'
 *      ],
 *      ...
 *  ];
 * }
 *
 * ...
 *
 * public function transactions()
 * {
 *
 *  return ['scenario' => self::OP_ALL];
 *
 * }
 *
 * ...
 *
 * public function getInputType($attribute)
 * {
 *  ...
 *
 *  case 'ImageGallery':
 *      $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_GALLERY_IMAGE;
 *      break;
 *  ...
 *
 * }
 *
 * ...
 *
 * public function getInputData($attribute)
 * {
 *  ...
 *
 *  case 'ImageGallery':
 *      $result = $this->getBehavior('imageGallery');
 *      break;
 *  ...
 *
 * }
 *
 * ...
 *
 * public function attributeLabels()
 * {
 *  ...
 *  'ImageGallery' => 'Галерея картинок',
 *  ...
 * }
 * ~~~
 *
 * @package amd_php_dev\yii2_components\behaviors
 */
class GalleryManager extends \yii\base\Behavior
{
    const TYPE_IMAGE = 'image';
    const TYPE_FILE  = 'file';
    const TYPE_VIDEO = 'video';

    /**
     * @var \amd_php_dev\yii2_components\models\SmartRecord
     */
    public $owner;

    /**
     * Тип галереи
     * @var string
     */
    public $type = self::TYPE_IMAGE;

    /**
     * Тип связи модели с элементами галереи
     * @var string
     */
    public $relationType = 'hasMany';

    /**
     * Имя класса модели элементов галереи
     * @var string
     */
    public $relationModelName;

    /**
     * Насройка ключей связи с элементами галереи
     *
     * ['id_item' => 'id']
     *
     * @var array
     */
    public $relationKeys = ['id_item' => 'id'];

    /**
     * Набор функций-условий для настройки запроса
     * к таблице элементов галереи
     *
     * [
     *  'scope1',
     *  'scope2' => ['param1', 'param2']
     * ]
     *
     * @var array
     */
    public $relationScopes = [
//        'active'
    ];

    /**
     * Имя модели-хозяина для формы
     * Model
     * @var string
     */
    public $ownerFormName;

    /**
     * Имя атрибута модели-хозяина для данной галереи
     * ImageGallery
     * @var string
     */
    public $galleryAttribute;

    /**
     * Имя модели-элемента галереи для формы
     * Model[ImageGallery]
     * @var string
     */
    public $galleryItemFormName;

    /**
     * Где хранить содержимое элемента галереи
     * @var string
     */
    public $galleryItemContentAttribute = 'content';

    /**
     * Выбранные элементы галереи
     * @var \amd_php_dev\yii2_components\models\gallery\GalleryItem[]|null|[]
     */
    protected $items;

    public function events()
    {
        return [
            \amd_php_dev\yii2_components\models\SmartRecord::EVENT_AFTER_INSERT  => 'afterSave',
            \amd_php_dev\yii2_components\models\SmartRecord::EVENT_AFTER_UPDATE  => 'afterSave',
            \amd_php_dev\yii2_components\models\SmartRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    public function canGetProperty($name, $checkVars = true)
    {
        if ($this->isGalleryAttribute($name)) {
            return true;
        }

        return parent::canGetProperty($name, $checkVars);
    }

    public function canSetProperty($name, $checkVars = true)
    {
        if ($this->isGalleryAttribute($name)) {
            return true;
        }

        return parent::canSetProperty($name, $checkVars);
    }

    public function isGalleryAttribute($name)
    {
        return $this->galleryAttribute == $name;
    }

    public function __set($name, $value)
    {
        if ($this->isGalleryAttribute($name)) {
            $this->setItems($value);
        } else {
            parent::__set($name, $value);
        }
    }

    public function __get($name)
    {
        if ($this->isGalleryAttribute($name)) {
            return $this->getItems();
        } else {
            return parent::__get($name);
        }
    }

    public function getNewItem()
    {
        $newItem = new $this->relationModelName;
        $newItem->id = md5(time() * rand(1, 100));
        return $this->constructModel($newItem);
    }

    public function attach($owner)
    {
        parent::attach($owner);

        $this->type = call_user_func([$this->relationModelName, 'getGalleryType']);
        $this->ownerFormName = $this->owner->formName();
        $this->galleryItemFormName = "{$this->ownerFormName}[{$this->galleryAttribute}]";
    }

    public function getLabel()
    {
        return $this->owner->getAttributeLabel($this->galleryAttribute);
    }

    /**
     * Возвращает элементы галереи
     *
     * @param bool $refresh принудительная выборка
     * @return \amd_php_dev\yii2_components\models\gallery\GalleryItem[]
     */
    public function getItems($refresh = false)
    {
        if (empty($this->items) || $refresh) {

            /**
             * @var $query \amd_php_dev\yii2_components\models\gallery\GalleryItemQuery
             */
            $query = $this->getItemsQuery();

            if (!empty($this->relationScopes)) {
                \amd_php_dev\yii2_components\helpers\DbHelper::scopeQuery($query, $this->relationScopes);
            }

            $this->items = $query->all();

            foreach($this->items as $item) {
                $item = $this->constructModel($item);
            }
        }

        return $this->items;
    }

    /**
     * Прикрепляет менеджер галереи и записывает id родителя для модели картинки
     * @param $model
     * @return mixed
     */
    public function constructModel($model)
    {
        $model->galleryManager = $this;
        if ($model->id_item != $this->owner->id) {
            $model->id_item = $this->owner->id;
        }
        return $model;
    }

    /**
     * Записывает в базу новые связанные данные
     * @param $items
     */
    public function setItems($items)
    {
        $notToDelete = [];

        $query = call_user_func([$this->relationModelName, 'find']);
        if (!empty($items)) {
            foreach ($items as $item) {
                if (
                    !empty($item['id'])
                    && $itemModel = $query->byPk($item['id'])->one()
                ) {
                    if ($itemModel->id_item != $this->owner->id) {
                        throw new BadResponseException('Нельзя редактировать чужие объекты!');
                    }
                } else {
                    $itemModel = new $this->relationModelName();
                }

                $itemModel->setAttributes($item);
                $itemModel = $this->constructModel($itemModel);
                if ($itemModel->save()) {
                    $notToDelete[] = $itemModel->id;
                }
            }
        }
        $deleteItems = $query->clean()->where(['not in', 'id', $notToDelete])->andWhere(['id_item' => $this->owner->id])->all();
        $this->deleteItems($deleteItems);
    }

    /**
     * Возвращает запрос к таблице элементов галереи
     */
    public function getItemsQuery()
    {
        return $this->owner->{$this->relationType}($this->relationModelName, $this->relationKeys);
    }

    /**
     * Выполняется после сохранения модели
     * @param $event
     */
    public function afterSave($event = null)
    {
        $ownerForm = \Yii::$app->request->post($this->ownerFormName);

        if (
            !empty($ownerForm)
            && (!empty($ownerForm[$this->galleryAttribute])
            || $this->owner->relationChange)
        ) {
            $items = (!empty($ownerForm[$this->galleryAttribute])) ? $ownerForm[$this->galleryAttribute] : null;
            $this->setItems($items);
        }
    }

    /**
     * Выполняется перед удалением модели
     *
     * Удаляет все связанные данные из галерей
     */
    public function beforeDelete()
    {
        $items = $this->getItems(true);
        $this->deleteItems($items);
    }

    /**
     * Удаляет переданные записи
     * @param $deleteItems
     */
    public function deleteItems($deleteItems)
    {
        foreach ($deleteItems as $item) {
            $item->delete();
        }
    }
}