<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 23.11.15
 * Time: 15:36
 */

namespace amd_php_dev\yii2_components\behaviors;


use Yii;
use yii\base\Exception;
use yii\base\InvalidCallException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 * Class FileUploadBehavior
 *
 * @property ActiveRecord $owner
 */
class FileUploadBehavior extends \yii\base\Behavior
{
    const EVENT_AFTER_FILE_SAVE = 'afterFileSave';

    /** @var string Name of attribute which holds the attachment. */
    public $attribute = 'upload';

    /** @var string Where to store images. */
    public $fileUrl = '/files/all/';

    public $webrootAlias = '@webroot';

    /**
     * Маска по которой создаётся название нового файла
     * @var string
     */
    public $fileNameMask = '[[hash]].[[extension]]';

    /**
     * Имя созданное из маски файла
     * @var string
     */
    public $fileName;

    public $hash;

    /**
     * @var string Attribute used to link owner model with it's parent
     * @deprecated Use attribute_xxx placeholder instead
     */
    public $parentRelationAttribute;

    /** @var \yii\web\UploadedFile */
    protected $file;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * Before validate event.
     */
    public function beforeValidate()
    {
        if ($this->owner->{$this->attribute} instanceof UploadedFile) {
            $this->file = $this->owner->{$this->attribute};
            return;
        }
        $this->file = UploadedFile::getInstance($this->owner, $this->attribute);

        if (empty($this->file)) {
            $this->file = UploadedFile::getInstanceByName($this->attribute);
        }
        if ($this->file instanceof UploadedFile) {
            $this->owner->{$this->attribute} = $this->file;
        }

    }

    /**
     * Before save event.
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeSave()
    {
        if ($this->file instanceof UploadedFile) {

            $this->owner->{$this->attribute} = $this->getFileUrl();

            if (!$this->owner->isNewRecord) {
                $this->cleanFiles();
            }

        } else { // Fix html forms bug, when we have empty file field
            if (!$this->owner->isNewRecord && empty($this->owner->{$this->attribute}))
                $this->owner->{$this->attribute} = ArrayHelper::getValue($this->owner->oldAttributes, $this->attribute, null);
        }
    }

    /**
     * After save event.
     */
    public function afterSave()
    {
        if ($this->file instanceof UploadedFile) {

            $path = $this->getFilePath();

            FileHelper::createDirectory(pathinfo($path, PATHINFO_DIRNAME), 0775, true);

            if (!$this->file->saveAs($path)) {
                throw new Exception('File saving error.');
            }

            $this->owner->trigger(static::EVENT_AFTER_FILE_SAVE);
        }
    }

    /**
     * Before delete event.
     */
    public function beforeDelete()
    {
        $this->cleanFiles();
    }

    public function cleanFiles()
    {
        if ($path = $this->owner->oldAttributes[$this->attribute]){
            @unlink($this->resolvePath($this->webrootAlias . $path));
        }

        $path = $this->owner->{$this->attribute};
        @unlink($this->resolvePath($this->webrootAlias . $path));
    }

    public function makeFileName()
    {
        return $this->resolvePath($this->fileNameMask);
    }

    public function getFileName()
    {
        if (empty($this->fileName)) {
            $this->fileName = $this->makeFileName();
        }
        return $this->fileName;
    }

    public function getFileUrl()
    {
        return $this->resolvePath($this->fileUrl . $this->getFileName());
    }

    public function getFilePath()
    {
        return $this->resolvePath($this->webrootAlias . $this->fileUrl . $this->getFileName());
    }

    /**
     * Replaces all placeholders in path variable with corresponding values
     *
     * @param string $path
     * @return string
     */
    public function resolvePath($path)
    {
        $path = Yii::getAlias($path, false);
        $pi = pathinfo($this->owner->{$this->attribute});
        $fileName = ArrayHelper::getValue($pi, 'filename');
        $extension = strtolower(ArrayHelper::getValue($pi, 'extension'));
        return preg_replace_callback('|\[\[([\w\_/]+)\]\]|', function ($matches) use ($fileName, $extension) {
            $name = $matches[1];
            switch ($name) {
                case 'extension':
                    return $extension;
                case 'filename':
                    return $fileName;
                case 'basename':
                    return  $fileName . '.' . $extension;
                case 'app_root':
                    return Yii::getAlias('@app');
                case 'web_root':
                    return Yii::getAlias('@webroot');
                case '@webroot':
                    return Yii::getAlias('@webroot');
                case 'hash':
                    return $this->getHash();
                case 'base_url':
                    return Yii::getAlias('@web');
                case 'model':
                    $r = new \ReflectionClass($this->owner->className());
                    return lcfirst($r->getShortName());
                case 'attribute':
                    return lcfirst($this->attribute);
                case 'id':
                case 'pk':
                    $pk = implode('_', $this->owner->getPrimaryKey(true));
                    return lcfirst($pk);
                case 'id_path':
                    return static::makeIdPath($this->owner->getPrimaryKey());
                case 'parent_id':
                    return $this->owner->{$this->parentRelationAttribute};
            }
            if (preg_match('|^attribute_(\w+)$|', $name, $am)) {
                $attribute = $am[1];
                return $this->owner->{$attribute};
            }
            return '[[' . $name . ']]';
        }, $path);
    }

    /**
     * @param integer $id
     * @return string
     */
    protected static function makeIdPath($id)
    {
        $id = is_array($id) ? implode('', $id) : $id;
        $length = 10;
        $id = str_pad($id, $length, '0', STR_PAD_RIGHT);
        $result = [];
        for ($i = 0; $i < $length; $i++)
            $result[] = substr($id, $i, 1);
        return implode('/', $result);
    }

    public function getHash()
    {
        if (empty($this->hash)) {
            $this->hash = md5(time() * rand());
        }

        return $this->hash;
    }
}