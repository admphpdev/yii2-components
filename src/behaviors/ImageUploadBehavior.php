<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 23.11.15
 * Time: 15:32
 */

namespace amd_php_dev\yii2_components\behaviors;

use \amd_php_dev\yii2_components\vendor\phpthumb\GD;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;


/**
 * Class ImageUploadBehavior
 */
class ImageUploadBehavior extends FileUploadBehavior
{
    public $attribute = 'image';
    public $createThumbsOnSave = false;

    /** @var array Thumbnail profiles, array of [width, height] */
    public $thumbsProfiles = [];

    public $thumbUrl = '/images/all/';

    public $fileUrl = '/images/all/';

    /**
     * @var integer Максимальные размеры изображения
     */
    public $maxWidth = 800;
    public $maxHeight = 600;

    /**
     * Создаёт имя thumb'ы из пути к файлу
     * @param string $filePath
     */
    public static function makeThumbName($filePath, $profile)
    {
        $ext = substr($filePath, strrpos($filePath, '.'));
        return str_replace($ext, "_{$profile}{$ext}", $filePath);
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return ArrayHelper::merge(parent::events(), [
            static::EVENT_AFTER_FILE_SAVE => 'afterFileSave',
        ]);
    }

    /**
     * After file save event handler.
     */
    public function afterFileSave()
    {
        if ($this->createThumbsOnSave == true)
            $this->createThumbs();
        if (!empty($this->maxHeight) && !empty($this->maxWidth))
            $this->resize();
    }

    /**
     * Меняет размер загруженного изображения на указанный
     * в параметрах $maxWidth и $maxHeight
     */
    public function resize() {
        $path = $this->getFilePath();
        if (is_file($path)) {
            $resize = new GD($path);
            $resize->adaptiveResize($this->maxWidth, $this->maxHeight);
            $resize->save($path);
        }
    }

    public function cleanFiles()
    {
        parent::cleanFiles();

        foreach (array_keys($this->thumbsProfiles) as $profile) {
            @unlink($this->getThumbFilePath($profile));
            if (!empty($this->owner->oldAttributes[$this->attribute])) {
                @unlink($this->getThumbFilePath($profile, true));
            }
        }
    }

    /**
     * Creates image thumbnails
     */
    public function createThumbs()
    {
        $path = $this->getFilePath();
        foreach ($this->thumbsProfiles as $profile => $config) {
            $thumbPath = $this->getThumbFilePath($profile);
            if (!is_file($thumbPath)) {
                /** @var GD $thumb */
                $thumb = new GD($path);
                $thumb->adaptiveResize($config['width'], $config['height']);
                FileHelper::createDirectory(pathinfo($thumbPath, PATHINFO_DIRNAME), 0775, true);
                $thumb->save($thumbPath);
            }
        }
    }

    public function getThumbFileName($profile)
    {
        $fileName = $this->getFileName();
        return self::makeThumbName($fileName, $profile);
    }

    public function getThumbFileUrl($profile, $old = false)
    {
        if ($old) {
            return self::makeThumbName($this->owner->oldAttributes[$this->attribute], $profile);
        }
        return $this->resolvePath($this->fileUrl . $this->getThumbFileName($profile));
    }

    public function getThumbFilePath($profile, $old = false)
    {
        if ($old) {
            return $this->resolvePath(
                $this->webrootAlias . self::makeThumbName($this->owner->oldAttributes[$this->attribute],
                    $profile)
            );
        }
        return $this->resolvePath($this->webrootAlias . $this->fileUrl . $this->getThumbFileName($profile));
    }
}