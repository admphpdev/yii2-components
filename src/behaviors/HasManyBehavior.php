<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 01.07.2016
 * Time: 9:36
 */

namespace amd_php_dev\yii2_components\behaviors;


class HasManyBehavior extends \yii\base\Behavior
{

    use SetableBehaviorTrait;

    /**
     * @var \amd_php_dev\yii2_components\models\SmartRecord
     */
    public $owner;

    public $setableAttribute;

    public $relationName;

    public $createObjects = true;

    public $defaultParentId = 0;

    public $childParentAttribute = 'id_parent';

    protected $_value;

    protected $_newValue;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            \yii\db\ActiveRecord::EVENT_AFTER_INSERT    => 'afterSave',
            \yii\db\ActiveRecord::EVENT_AFTER_UPDATE    => 'afterSave',
            \yii\db\ActiveRecord::EVENT_BEFORE_DELETE   => 'beforeDelete',
        ];
    }

    public function getValue()
    {
        return !empty($this->_newValue) ? $this->_newValue : $this->getCurrentValue();
    }

    public function setValue($value)
    {
        $this->_newValue = $this->filterValue($value);
    }

    public function addValue($value)
    {
        $this->_newValue = array_merge(
            $this->filterValue($this->getValue()),
            $this->filterValue($value)
        );
    }

    public function filterValue($value)
    {
        $filtredValue = [];
        foreach ($value as $key => $val) {
            if (is_array($val)) {
                $filtredValue[] = $val['id'];
            } elseif (is_numeric($val)) {
                $filtredValue[] = (int) $val;
            }
        }

        return $filtredValue;
    }

    public function getCurrentValue($refresh = false)
    {
        if (empty($this->_value) || $refresh) {
            $values = $this->owner->getRelation($this->relationName)->all();
            $this->_value = $this->filterValue($values);
            return $values;
        }

        return $this->_value;
    }

    public function afterSave()
    {
        if (
            property_exists($this->owner, 'relationChange') &&
            $this->owner->relationChange == true
        ) {
            if (empty($this->_newValue)) {
                $this->_newValue = [];
            }
        } elseif (property_exists($this->owner, 'relationChange')) {
            if (empty($this->_newValue)) {
                return;
            }
        }

        if ($this->_newValue === null) {
            return;
        }

        $deleteItems = $this->getToDelete();

        if (!empty($deleteItems) && is_array($deleteItems)) {
            if ($this->createObjects) {
                $this->deleteForObjects($deleteItems);
            } else {
                $this->deleteForNonObjects($deleteItems);
            }
        }

        if (!empty($this->_newValue)) {
            if ($this->createObjects) {
                $this->updateForObjects();
            } else {
                $this->updateForNonObjects();
            }
        }
    }

    protected function getToDelete()
    {
        $newItems = $this->_newValue;
        $currentItems = $this->owner->getRelation($this->relationName)->select('id')->asArray()->all();

        foreach ($currentItems as $key => $currentItem) {
            $currentItems[$key] = $currentItem['id'];
        }

        if (empty($newItems) || !is_array($newItems)) {
            $deleteItems = $currentItems;
        } else {
            $deleteItems = array_filter(
                $currentItems,
                function ($e) use($newItems) {
                    return !in_array($e, $newItems);
                }
            );
        }

        return $deleteItems;
    }

    public function beforeDelete()
    {
        if ($this->createObjects) {
            $this->deleteForObjects();
        } else {
            $this->deleteForNonObjects();
        }
    }

    protected function updateForObjects()
    {
        /**
         * @var $items \amd_php_dev\yii2_components\models\SmartRecord[]
         */
        $items = $this->owner->getRelation($this->relationName)
            ->clean()
            ->where(['id' => $this->_newValue])
            ->all();

        foreach ($items as $item) {
            $item->relationChange = false;
            $item->{$this->childParentAttribute} = (int) $this->owner->id;
            $item->save();
        }
    }

    protected function updateForNonObjects()
    {
        $ids = \amd_php_dev\yii2_components\helpers\DbHelper::quoteArray($this->_newValue);

        $relation = $this->owner->getRelation($this->relationName);

        $pivot = $relation->primaryModel->tableName();
        $this->owner->getDb()
            ->createCommand()
            ->update(
                $pivot,
                [$this->childParentAttribute => (int) $this->owner->id],
                "id IN ({$ids})"
            )
            ->execute();
    }

    protected function deleteForObjects($deleteItems = null)
    {
        if (!empty($deleteItems) && is_array($deleteItems)) {
            $relatedItems = $this->owner->getRelation($this->relationName)
                ->clean()->where(['id' => $deleteItems])->all();
        } else {
            $relatedItems = $this->owner->getRelation($this->relationName)->all();
        }

        /**
         * @var $relatedItems \amd_php_dev\yii2_components\models\SmartRecord[]
         */
        foreach ($relatedItems as $relatedItem) {
            $relatedItem->relationChange = false;
            $relatedItem->{$this->childParentAttribute} = (int) $this->defaultParentId;
            $relatedItem->save();
        }
    }

    protected function deleteForNonObjects($deleteItems = null)
    {
        $relation = $this->owner->getRelation($this->relationName);

        $pivot = $relation->primaryModel->tableName();

        if (!empty($deleteItems) && is_array($deleteItems)) {
            $ids = \amd_php_dev\yii2_components\helpers\DbHelper::quoteArray($deleteItems);
            $condition = "id IN ({$ids})";
        } else {
            $condition = "{$this->childParentAttribute} = {$this->owner->id}";
        }

        $this->owner->getDb()
            ->createCommand()
            ->update(
                $pivot,
                [$this->childParentAttribute => (int) $this->defaultParentId],
                $condition
            )
            ->execute();
    }
}