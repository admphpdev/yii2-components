<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 11.05.16
 * Time: 12:11
 */

namespace amd_php_dev\yii2_components\behaviors;


trait SetableBehaviorTrait
{
    public function canGetProperty($name, $checkVars = true)
    {
        if ($this->isSetableAttribute($name)) {
            return true;
        }

        return parent::canGetProperty($name, $checkVars);
    }

    public function canSetProperty($name, $checkVars = true)
    {
        if ($this->isSetableAttribute($name)) {
            return true;
        }

        return parent::canSetProperty($name, $checkVars);
    }

    public function isSetableAttribute($name)
    {
        return $this->setableAttribute == $name;
    }

    public function __set($name, $value)
    {
        if ($this->isSetableAttribute($name)) {
            $this->setValue($value);
        } else {
            parent::__set($name, $value);
        }
    }

    public function __get($name)
    {
        if ($this->isSetableAttribute($name)) {
            return $this->getValue();
        } else {
            return parent::__get($name);
        }
    }

    abstract public function getValue();

    abstract public function setValue($value);

    abstract public function getCurrentValue($refresh = false);

    abstract public function filterValue($value);
}