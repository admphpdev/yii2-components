<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 11.05.16
 * Time: 12:05
 */

namespace amd_php_dev\yii2_components\behaviors;


class GalleryManager2 extends \yii\base\Behavior
{
    const TYPE_IMAGE = 'image';
    const TYPE_FILE  = 'file';
    const TYPE_VIDEO = 'video';

    /**
     * @var \amd_php_dev\yii2_components\models\SmartRecord
     */
    public $owner;

    /**
     * Тип галереи
     * @var string
     */
    public $type = self::TYPE_IMAGE;

    public $valueRelation = 'galleryItemsRelation';

    /**
     * Имя модели-хозяина для формы
     * Model
     * @var string
     */
    public $ownerFormName;

    /**
     * Имя модели-элемента галереи для формы
     * Model[ImageGallery]
     * @var string
     */
    public $galleryItemFormName;

    /**
     * Где хранить содержимое элемента галереи
     * @var string
     */
    public $galleryItemContentAttribute = 'content';

    /**
     * Имя атрибута модели-хозяина для данной галереи
     * ImageGallery
     * @var string
     */
    public $setableAttribute = 'galleryItems';

    public $galleryAttribute = 'galleryItems';

    /**
     * Выбранные элементы галереи
     * @var \amd_php_dev\yii2_components\models\gallery\GalleryItem[]|null|[]
     */
    protected $_value;

    protected $_newValue = [];

    public function events()
    {
        return [
            \amd_php_dev\yii2_components\models\SmartRecord::EVENT_AFTER_INSERT  => 'afterSave',
            \amd_php_dev\yii2_components\models\SmartRecord::EVENT_AFTER_UPDATE  => 'afterSave',
            \amd_php_dev\yii2_components\models\SmartRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    use SetableBehaviorTrait;

    public function getValue()
    {
        return !empty($this->_newValue) ? $this->_newValue : $this->getCurrentValue();
    }

    public function setValue($value)
    {
        $this->_newValue = $this->filterValue($value);
    }

    public function getCurrentValue($refresh = false)
    {
        if (empty($this->_value) || $refresh) {
            $this->_value = $this->owner->getRelation($this->valueRelation)->all();

            foreach ($this->_value as $key => $value) {
                $this->_value[$key] = $this->constructModel($value);
            }
        }

        return $this->_value;
    }

    /**
     * Выполняется перед удалением модели
     *
     * Удаляет все связанные данные из галерей
     */
    public function beforeDelete()
    {
        $items = $this->getCurrentValue();
        $this->deleteItems($items);
    }

    /**
     * Удаляет переданные записи
     * @param $deleteItems
     */
    public function deleteItems($deleteItems)
    {
        foreach ($deleteItems as $item) {
            $item->delete();
        }
    }

    /**
     * Прикрепляет менеджер галереи и записывает id родителя для модели картинки
     * @param $model
     * @return mixed
     */
    public function constructModel($model)
    {
        $model->galleryManager = $this;
        if ($model->id_item != $this->owner->id) {
            $model->id_item = $this->owner->id;
        }
        return $model;
    }

    public function filterValue($value)
    {
        $filtredValues = [];

        foreach ($value as $val) {
            if (!empty($val['id'])) {
                $filtredValues[$val['id']] = $val;
            }
        }

        return $filtredValues;
    }

    public function getLabel()
    {
        return $this->owner->getAttributeLabel($this->galleryAttribute);
    }

    public function attach($owner)
    {
        parent::attach($owner);
        $this->ownerFormName = $this->owner->formName();
        $this->galleryAttribute = $this->setableAttribute;
        $this->galleryItemFormName = "{$this->ownerFormName}[{$this->galleryAttribute}]";
    }

    public function getItems($refresh = false)
    {
        return $this->getValue($refresh = false);
    }

    public function afterSave()
    {
        if (
            property_exists($this->owner, 'relationChange') &&
            $this->owner->relationChange == true
        ) {
            if (empty($this->_newValue)) {
                $this->_newValue = [];
            }
        } elseif (property_exists($this->owner, 'relationChange')) {
            if (empty($this->_newValue)) {
                return;
            }
        }

        if ($this->_newValue === null) {
            return;
        }

        $notToDelete = [];

        foreach ($this->_newValue as $key => $value) {
            if (is_numeric($key)) {
                $notToDelete[] = (int) $key;
            }
        }

        $deleteItems = $this->owner->getRelation($this->valueRelation)
            ->clean()
            ->where(['not in', 'id', $notToDelete])
            ->andWhere(['id_item' => $this->owner->id])
            ->all();

        $this->deleteItems($deleteItems);

        $updateItems = $this->owner->getRelation($this->valueRelation)
            ->clean()
            ->byPk($notToDelete)
            ->indexBy('id')
            ->all();

        foreach ($this->_newValue as $key => $value) {
            if (!empty($updateItems[$key])) {
                $item = $updateItems[$key];
            } else {
                $item = $this->getNewItem();
            }

            $item->setAttributes($value);
            $item = $this->constructModel($item);
            $item->save();
        }
    }

    public function getNewItem()
    {
        $relation = $this->owner->getRelation($this->valueRelation);
        $newItem = new $relation->modelClass;
        $newItem->id = md5(time() * rand(1, 100));
        return $this->constructModel($newItem);
    }
}