<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 26.04.16
 * Time: 10:56
 */

namespace amd_php_dev\yii2_components\behaviors;


class OptionBehavior extends \yii\base\Behavior
{
    /**
     * @var \amd_php_dev\yii2_components\models\SmartRecord
     */
    public $owner;

    /**
     * @var \amd_php_dev\yii2_components\models\Option[]
     */
    public $options;
    /**
     * @var \amd_php_dev\yii2_components\models\Option[]
     */
    public $optionsByCode = [];
    /**
     * @var \amd_php_dev\yii2_components\models\Option[]
     */
    public $optionsByIdGroup = [];
    /**
     * @var \amd_php_dev\yii2_components\models\OptionGroup[]
     */
    public $optionGroups;
    /**
     * @var \amd_php_dev\yii2_components\models\OptionGroup[]
     */
    public $optionGroupsByCode = [];
    public $valueFormModelClass = '\amd_php_dev\yii2_components\models\OptionValueForm';
    public $setableAttribute = 'optionValues';
    public $optionsRelation = 'optionsRelation';
    /**
     * false по умолчанию
     * 'optionGroupsRelation'
     * @var string
     */
    public $optionGroupsRelation = false;
    public $optionValuesRelation = 'optionValuesRelation';

    public $errors = [];

    protected $_values;

    protected $_newValues;

    use SetableBehaviorTrait;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            \yii\db\ActiveRecord::EVENT_AFTER_INSERT    => 'afterSave',
            \yii\db\ActiveRecord::EVENT_AFTER_UPDATE    => 'afterSave',
            \yii\db\ActiveRecord::EVENT_AFTER_VALIDATE  => 'afterValidate',
            \yii\db\ActiveRecord::EVENT_BEFORE_DELETE   => 'beforeDelete',
        ];
    }

    public function getValue()
    {
        return !empty($this->_newValues) ? $this->_newValues : $this->getCurrentValue();
    }

    public function setValue($value)
    {
        $this->_newValues = $this->filterValue($value);
    }

    public function addValue($value)
    {
        $this->_newValues = array_merge($this->filterValue($this->getValue()), $this->filterValue($value));
    }

    public function filterValue($value)
    {
        $filtredValues = [];
        foreach ($value as $key => $val) {
            if (is_array($val)) {
                $filtredValues[$key] = $val['value'];
            } else {
                $filtredValues[$key] = $val;
            }
        }

        return $filtredValues;
    }

    public function getOptionsByPks($pks)
    {
        if (empty($pks) || !is_array($pks)) {
            return null;
        }

        $pks = array_map(
            function ($e) { return (int) $e; },
            $pks
        );

        return $this->owner->getRelation($this->optionsRelation)
            ->clean()
            ->defaultScope()
            ->andWhere(['in', 'id', $pks])
            ->indexBy('id')
            ->all();
    }

    public function getOptions()
    {
        if (empty($this->options)) {
            $this->options = $this->owner->getRelation($this->optionsRelation)->all();
            $this->indexOptions();
        }
        return $this->options;
    }

    public function indexOptions()
    {
        foreach ($this->options as $option) {
            $option->optionsManager = $this;
            $this->optionsByCode[$option->code] = $option;
            if (!empty($this->getOptionGroups())) {
                $this->optionsByIdGroup[$option->id_group][$option->id] = $option;
            }
        }
    }

    public function getOptionsBy($index, $value)
    {
        $result = null;

        if (empty($this->getOptions()) || empty($value)) {
            return $result;
        }

        switch ($index) {
            case 'id_group' :
                if (empty($this->optionsByIdGroup[$value])) {
                    $this->indexOptions();
                }
                $result = (!empty($this->optionsByIdGroup[$value])) ? $this->optionsByIdGroup[$value] : null;
                break;
            case 'code' :
                if (empty($this->optionsByCode[$value])) {
                    $this->indexOptions();
                }
                $result = (!empty($this->optionsByCode[$value])) ? $this->optionsByCode[$value] : null;
                break;
        }

        return $result;
    }

    public function getOptionValueBy($index, $value)
    {
        $result = null;
        $option = $this->getOptionsBy($index, $value);
        if (!empty($option)) {
            $result = $option->getOptionValue();
        }
        return $result;
    }

    public function getOptionGroups()
    {
        if (empty($this->optionGroupsRelation)) {
            return null;
        }

        if (empty($this->optionGroups) && !empty($this->optionGroupsRelation)) {
            $this->optionGroups = $this->owner->getRelation($this->optionGroupsRelation)->all();
            foreach ($this->optionGroups as $optionGroup) {
                $this->optionGroupsByCode[$optionGroup->code] = $optionGroup;
            }
        }
        return $this->optionGroups;
    }

    public function getCurrentValue($refresh = false)
    {
        if (empty($this->_values) || $refresh) {
            $values = $this->owner->{$this->optionValuesRelation}->all();
            $this->_values = $this->filterValue($values);
            return $values;
        }

        return $this->_values;
    }

    public function validateOptionValue($option)
    {
        if (!empty($option->default) && empty($this->_newValues[$option->id])) {
            $this->_newValues[$option->id] = $option->default;
        }

        $formModel = new $this->valueFormModelClass;
        $value = !empty($this->_newValues[$option->id]) ? $this->_newValues[$option->id] : null;

        $formModel->setData(
            $this->owner,
            $option,
            $this->setableAttribute,
            $value
        );

        $valid = $formModel->validate();
        if (!$valid) {
            $this->errors[$option->id] = $formModel->getErrors();
        }

        return $valid;
    }

    public function afterSave()
    {
        if ($this->_newValues === null) {
            return;
        }

        if (!$this->owner->getIsNewRecord()) {
            $this->beforeDelete();
        }

        if (
            property_exists($this->owner, 'relationChange') &&
            $this->owner->relationChange == true &&
            empty($this->_newValues)
        ) {
            $this->_newValues = [];
        }
        $valuesRelation = $this->owner->{$this->optionValuesRelation};
        $pivot = $valuesRelation->from[0];
        $rows = [];

        foreach ($this->_newValues as $optionId => $value) {
            if (empty($value)) {
                continue;
            }

            $rows[] = [
                'id_item' => $this->owner->getPrimaryKey(),
                'id_option' => (int) $optionId,
                'value' => $value
            ];
        }

        if (!empty($rows)) {
            $this->owner->getDb()
                ->createCommand()
                ->batchInsert($pivot, array_keys($rows[0]), $rows)
                ->execute();
        }
    }

    public function beforeDelete()
    {
        $relation = $this->owner->{$this->optionValuesRelation};

        $pivot = $relation->from[0];
        $this->owner->getDb()
            ->createCommand()
            ->delete($pivot, ['id_item' => $this->owner->getPrimaryKey()])
            ->execute();
    }

    public function afterValidate()
    {
        if (empty($this->_newValues)) {
            return null;
        }

        $this->errors = [];

        $options = $this->getOptionsByPks(array_keys($this->_newValues));
        foreach ($options as $option) {
            $this->validateOptionValue($option);
        }

        if (!empty($this->errors)) {
            $this->owner->addError($this->setableAttribute, $this->errors);
        }
    }
}