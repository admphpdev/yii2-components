<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 07.12.15
 * Time: 18:42
 */

namespace amd_php_dev\yii2_components\behaviors;
use \yii\db\ActiveRecord;

class AuthorBehavior extends \yii\base\Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            //ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    public function beforeSave()
    {
        if ((\Yii::$app instanceof Application) && !empty(\Yii::$app->user->id)) {
            $this->owner->author = \Yii::$app->user->id;
        }
    }

}