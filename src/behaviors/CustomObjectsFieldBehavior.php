<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 14.12.15
 * Time: 11:21
 */

namespace amd_php_dev\yii2_components\behaviors;
use yii\db\ActiveRecord;

/**
 * Class CustomObjectsFieldBehavior
 * Поведение для работы с произвольными объектами
 * записанными в поле модели при помощи json
 * @package amd_php_dev\yii2_components\behaviors
 */
class CustomObjectsFieldBehavior extends \yii\base\Behavior
{
    /**
     * Базовый класс
     * @var string
     */
    public $objectClass = '\amd_php_dev\yii2_components\models\CustomJsonObject';

    /**
     * Атрибут модели, в которой хранится JSON
     * @var string
     */
    public $objectsField;

    /**
     * Атрибуты объекта
     * @var array
     */
    public $objectAttributes = [];

    protected $_baseFormName;

    /**
     * Получает объект и записывает в базу в виде JSON
     * @param mixed $object
     * @return mixed
     */
    public function setObjects($objects)
    {
        $this->owner->{$this->objectsField} = $this->serialize($objects);
    }

    /**
     * Возвращает объект
     * @return mixed
     */
    public function getObjects()
    {
        return $this->unserialize($this->owner->{$this->objectsField}, $this->objectClass);
    }

    /**
     * Сериализует принятый объект в JSON
     * @param mixed $object
     * @return array|string
     */
    public function serialize($objects)
    {
        $result = [];

        foreach ($objects as $object) {
            $objArray = [];
            foreach ($this->objectAttributes as $attr => $desc) {
                if (count($this->objectAttributes) == 1) {
                    if (!empty($object[$attr])) {
                        $objArray[$attr] = $object[$attr];
                    }
                } elseif (isset($object[$attr])) {
                    $objArray[$attr] = $object[$attr];
                }
            }

            if (count($objArray) == count($this->objectAttributes)) {
                $result[] = $objArray;
            }
        }

        $result = json_encode($result);
        return $result;
    }

    /**
     * Создает объект заданного класса из JSON
     * @param string $json
     * @param string $class
     * @return mixed
     */
    public function unserialize($json, $class)
    {
        $result = [];

        $jsonArr = json_decode($json);

        if (empty($jsonArr)) {
            return false;
        }

        foreach ($jsonArr as $index => $item) {

            $attributes = [];
            foreach((array)$item as $key => $value) {
                if (array_key_exists($key, $this->objectAttributes)) {
                    $attributes[$key] = $value;
                }
            }

            if (empty($attributes)) {
                continue;
            }

            $attributes['serialIndex']   = $index;

            $class = (!empty($item->class)) ? $item->class : null;

            $result[] = $this->getObject($attributes, $class);
        }

        return $result;
    }

    public function getObject($attributes = [], $class = null)
    {
        if (!empty($class)) {
            $objectClass = $class;
        } elseif (!empty($this->objectClass)) {
            $objectClass = $this->objectClass;
        } else {
            $objectClass = null;
        }


        $attributes['baseFormName']  = $this->getBaseFormName();
        $attributes['objectManager'] = $this;

        return ($objectClass) ? new $objectClass ($attributes) : (object) $attributes;
    }

    public function getBaseFormName()
    {
        if (empty($this->_baseFormName))
            $this->_baseFormName = $this->owner->formName() . '[' . $this->objectsField . ']';

        return $this->_baseFormName;
    }

    public function events()
    {
        return [ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate'];
    }

    public function beforeValidate()
    {
        if (!(\Yii::$app instanceof \yii\console\Application)) {
            $post = \Yii::$app->getRequest()->post();
            if (!empty($post[$this->owner->formName()][$this->objectsField])) {

                $this->setObjects($post[$this->owner->formName()][$this->objectsField]);
            } elseif (isset($post[$this->owner->formName()][$this->objectsField]) && empty($post[$this->owner->formName()][$this->objectsField])) {
                $this->owner->{$this->objectsField} = null;
            }
        }
    }
}