<?php
namespace amd_php_dev\yii2_components\migrations;

use \yii\db\Schema;
use \yii\base\ErrorException;


/**
 * Description of Migration
 *
 * @author nofuture17
 */
class Migration extends \yii\db\Migration
{

    public $tableOptions;

    public function init()
    {
        parent::init();

        $this->tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
    }

    public function getFullTableName($tableName) {
        return $this->db->tablePrefix . $this->getCleanTableName($tableName);
    }

    //{{%tableName}}
    public function getCleanTableName($tableName) {
        return trim($tableName, '{%}');
    }

    /**
     * Создаёт связь MANY_MANY между таблицами
     * @param string $tableName Таблица которую связываем
     * @param string $tableIndexColumn Название поля для связи
     * @param string $refTableName Таблица с которой связываем
     * @param string $refTableIndexColumn Название поля для связи
     * @param array|null $additionalColumns Дополнительные колонки
     * @param array|null $options Опции
     */
    public function createRelationTable(
        $tableName,
        $tableIndexColumn,
        $refTableName,
        $refTableIndexColumn,
        $additionalColumns = null,
        $additionalIndexes = null,
        $options = null
    ) {
        $relationTableName  = $this->getRelationTableName($tableName, $refTableName);

        $relationTableColumns =  [
            'id' => $this->primaryKey(),
            $tableIndexColumn => $this->integer()->notNull(),
            $refTableIndexColumn => $this->integer()->notNull()
        ];

        if (!empty($additionalColumns)) {
            $relationTableColumns = \yii\helpers\ArrayHelper::merge($additionalColumns, $relationTableColumns);
        }


        $this->createTable($relationTableName, $relationTableColumns, $options);

        $indexes = [
            'tableIndex' => [
                'name'          => 'idx_' . $relationTableName . '_' . $tableIndexColumn,
                'table'         => $relationTableName,
                'columns'       => $tableIndexColumn,
                'unique'        => true
            ],
            'refTableIndex' => [
                'name'          => 'idx_' . $relationTableName . '_' . $refTableIndexColumn,
                'table'         => $relationTableName,
                'columns'       => $refTableIndexColumn,
                'unique'        => true
            ],
        ];

        if (!empty($additionalIndexes) && is_array($additionalIndexes)) {
            $indexes = \yii\helpers\ArrayHelper::merge($additionalIndexes, $indexes);
        }

        $this->createIndexes($indexes);
    }

    public function getIndexTemplate($tableName, $indexName, $params = []) {
        return [
            'name'          => 'idx_' . $this->getFullTableName($tableName) . '_' . $indexName,
            'table'         => $tableName,
            'columns'       => $indexName,
            'unique'        => empty($params['unique']) ? false : true
        ];
    }

    /**
     * Получить имя таблицы связи MANY_MANY
     * @param string $tableName
     * @param string $refTableName
     * @return string
     */
    public function getRelationTableName($tableName, $refTableName) {
        // Чистим имена таблиц
        $tableName = $this->getCleanTableName($tableName);
        $refTableName = $this->getCleanTableName($refTableName);

        return $this->getFullTableName($tableName) . '_to_' . $refTableName;
    }

    /**
     * Создаёт идексы из массива
     * @param array $indexes
     */
    public function createIndexes($indexes) {
        foreach ($indexes as $name => $params) {
            $this->createIndex(
                $params['name'],
                $params['table'],
                $params['columns'],
                isset($params['unique']) ? $params['unique'] : false
            );
        }
    }
}
