<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 29.03.16
 * Time: 12:05
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class TagRelation extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы связи с тегами
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'id_tag'            => $this->_migration->integer(),
            'id_item'           => $this->_migration->integer(),
        ];

        return $columns;
    }

    /**
     * Возвращает шаблон индексов таблицы связи с тегами
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'id_tag' => $this->_migration->getIndexTemplate($this->tableName, 'id_tag'),
            'id_item' => $this->_migration->getIndexTemplate($this->tableName, 'id_item'),
        ];

        return $indexes;
    }
}