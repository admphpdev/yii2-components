<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 14:21
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class File extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы файлов
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'id_item'           => $this->_migration->integer(),
            'name'              => $this->_migration->string(255),
            'path'              => $this->_migration->string(255),
            'ext'               => $this->_migration->string(32),
            'type'              => $this->_migration->string(32),
            'priority'          => $this->_migration->integer(),
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы файлов
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'id_item' => $this->_migration->getIndexTemplate($this->tableName, 'id_item'),
            'priority' => $this->_migration->getIndexTemplate($this->tableName, 'priority')
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}