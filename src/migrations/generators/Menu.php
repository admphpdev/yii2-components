<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 07.12.15
 * Time: 13:54
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class Menu extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы меню
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'active'            => $this->_migration->integer()->defaultValue(0),
            'section'           => $this->_migration->string(255),
            'name'              => $this->_migration->string(255),
            'author'            => $this->_migration->integer()->defaultValue(0)
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы меню
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'active' => $this->_migration->getIndexTemplate($this->tableName, 'active'),
            'section' => $this->_migration->getIndexTemplate($this->tableName, 'section'),
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}