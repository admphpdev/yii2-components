<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 26.04.16
 * Time: 10:56
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class OptionGroup extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы групп опций
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'id_parent'         => $this->_migration->integer()->defaultValue(0),
            'code'              => $this->_migration->string(255),
            'image'             => $this->_migration->string(255),
            'name'              => $this->_migration->string(255),
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы групп опций
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'code'          => $this->_migration->getIndexTemplate($this->tableName, 'code'),
            'id_parent'     => $this->_migration->getIndexTemplate($this->tableName, 'id_parent'),
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }

}