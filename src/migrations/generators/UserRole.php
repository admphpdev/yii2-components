<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 02.06.2016
 * Time: 14:38
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class UserRole extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы пользователей
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                    => $this->_migration->primaryKey(),
            'role'                  => $this->_migration->string(32),
            'parent'                => $this->_migration->string(32),
            'name'                  => $this->_migration->string(255),
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы пользователей
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'role'          => $this->_migration->getIndexTemplate($this->tableName, 'role'),
            'parent'        => $this->_migration->getIndexTemplate($this->tableName, 'parent'),
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}