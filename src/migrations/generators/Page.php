<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 9:42
 */

namespace amd_php_dev\yii2_components\migrations\generators;

/**
 * Class Page
 * Генерирует таблицы и индексы для страниц
 * @package app\migrations\generators
 */
class Page extends Generator
{
    /**
     * Возвращает стандартный шаблон страницы
     * @param string $parent Имя колонки для связи BELONGS_TO
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'created_at'        => $this->_migration->integer(),
            'updated_at'        => $this->_migration->integer(),
            'priority'          => $this->_migration->smallInteger()->defaultValue(500),
            'active'            => $this->_migration->smallInteger(),
            'author'            => $this->_migration->smallInteger(),
            'name'              => $this->_migration->string(255),
            'name_small'        => $this->_migration->string(255),
            'url'               => $this->_migration->string(255),
            'meta_title'        => $this->_migration->string(255),
            'meta_keywords'     => $this->_migration->string(255),
            'meta_description'  => $this->_migration->string(255),
            'text_small'        => $this->_migration->text(),
            'text_full'         => $this->_migration->text(),
            'links'             => $this->_migration->text(),
            'snipets'           => $this->_migration->text(),
            'image_small'       => $this->_migration->string(255),
            'image_full'        => $this->_migration->string(255)
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает массив стандартных идексов
     * @return array
     */
    public function getIndexesTemplate() {

        $indexes = [
            'active'        => $this->_migration->getIndexTemplate($this->tableName, 'active'),
            'url'           => $this->_migration->getIndexTemplate($this->tableName, 'url'),
            'author'        => $this->_migration->getIndexTemplate($this->tableName, 'author'),
            'priority'      => $this->_migration->getIndexTemplate($this->tableName, 'priority')
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}