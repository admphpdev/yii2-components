<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 11:49
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class OptionValue extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы значений опций
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id_option' => $this->_migration->smallInteger(),
            'id_item'   => $this->_migration->smallInteger(),
            'value'     => $this->_migration->text(),
        ];

        return $columns;
    }

    /**
     * Возвращает шаблон индексов таблицы значений опций
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
        ];

        return $indexes;
    }

    public function create()
    {
        parent::create();
        $this->_migration->addPrimaryKey('', $this->tableName, ['id_item', 'id_option']);
    }
}