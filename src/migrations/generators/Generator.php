<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 11:44
 */

namespace amd_php_dev\yii2_components\migrations\generators;
use \amd_php_dev\yii2_components\migrations\Migration;

class Generator
{
    public $tableName;
    public $primaryKey = 'id';
    public $additionalColumns = [];
    public $additionsIndexes = [];

    /**
     * @var \amd_php_dev\yii2_components\migrations\Migration;
     */
    protected $_migration;
    protected $_columns;
    protected $_indexes;

    public function __construct(Migration $migration, $tableName)
    {
        $this->_migration   = $migration;
        $this->tableName    = $tableName;
    }

    /**
     * Создаёт таблицу и индексы вбазе данных
     */
    public function create() {
        $columns = \yii\helpers\ArrayHelper::merge($this->additionalColumns, $this->getColumnsTemplate());
        $indexes = \yii\helpers\ArrayHelper::merge($this->additionsIndexes, $this->getIndexesTemplate());
        $this->_migration->createTable($this->tableName, $columns, $this->_migration->tableOptions);
        $this->_migration->createIndexes($indexes);
    }

    /**
     * Добавляет дополнительный индекс
     * @param string $indexName
     * @param array|null $params
     */
    public function addIndex($indexName, $params = null) {
        $this->additionsIndexes[$indexName] = $this->_migration->getIndexTemplate($this->tableName, $indexName, $params);
    }

    /**
     * Возвращает шаблон колонок таблицы
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id' => $this->_migration->primaryKey(),
            'active' => $this->_migration->integer(),
            'priority' => $this->_migration->integer(),
        ];

        return $columns;
    }

    /**
     * Возвращает шаблон индексов таблицы
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'active' => $this->_migration->getIndexTemplate($this->tableName, 'active'),
            'priority' => $this->_migration->getIndexTemplate($this->tableName, 'priority')
        ];

        return $indexes;
    }
}