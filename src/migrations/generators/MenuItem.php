<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 07.12.15
 * Time: 13:55
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class MenuItem extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы элементов меню
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'active'            => $this->_migration->integer()->defaultValue(0),
            'name'              => $this->_migration->string(255),
            'url'               => $this->_migration->string(255),
            'id_menu'           => $this->_migration->integer(),
            'priority'          => $this->_migration->integer()->defaultValue(100),
            'id_parent'         => $this->_migration->integer()->defaultValue(0),
            'author'            => $this->_migration->integer()->defaultValue(0),
            'linkOptions'       => $this->_migration->string(255),
            'options'           => $this->_migration->string(255),
            'dropDownOptions'   => $this->_migration->string(255),
            'lft'               => $this->_migration->integer(),
            'rgt'               => $this->_migration->integer(),
            'depth'             => $this->_migration->integer(),
            'tree'              => $this->_migration->integer(),
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы элементов меню
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'active'    => $this->_migration->getIndexTemplate($this->tableName, 'active'),
            'priority'  => $this->_migration->getIndexTemplate($this->tableName, 'priority'),
            'id_menu'   => $this->_migration->getIndexTemplate($this->tableName, 'id_menu'),
            'id_parent' => $this->_migration->getIndexTemplate($this->tableName, 'id_parent'),
            'lft'       => $this->_migration->getIndexTemplate($this->tableName, 'lft'),
            'rgt'       => $this->_migration->getIndexTemplate($this->tableName, 'rgt'),
            'depth'     => $this->_migration->getIndexTemplate($this->tableName, 'depth'),
            'tree'      => $this->_migration->getIndexTemplate($this->tableName, 'tree'),
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}