<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 04.12.15
 * Time: 19:43
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class Redirect extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы редиректов
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'active'            => $this->_migration->integer()->defaultValue(0),
            'code'              => $this->_migration->integer()->defaultValue(301),
            'from'              => $this->_migration->string(255),
            'to'                => $this->_migration->string(255),
            'author'            => $this->_migration->integer()->defaultValue(0)
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы редиректов
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'active' => $this->_migration->getIndexTemplate($this->tableName, 'active'),
            'from' => $this->_migration->getIndexTemplate($this->tableName, 'from'),
            'to'  => $this->_migration->getIndexTemplate($this->tableName, 'to'),
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}