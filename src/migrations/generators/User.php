<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 14:46
 */

namespace amd_php_dev\yii2_components\migrations\generators;

use \amd_php_dev\yii2_components\models\User as UserModel;

class User extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы пользователей
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                    => $this->_migration->primaryKey(),
            'created_at'            => $this->_migration->integer(),
            'updated_at'            => $this->_migration->integer(),
            'username'              => $this->_migration->string(),
            'auth_key'              => $this->_migration->string(32),
            'email_confirm_token'   => $this->_migration->string(),
            'password_hash'         => $this->_migration->string(),
            'password_reset_token'  => $this->_migration->string(),
            'email'                 => $this->_migration->string(),
            'login'                 => $this->_migration->string(),
            'role'                  => $this->_migration->string(32)->defaultValue(UserModel::ROLE_USER),
            'active'                => $this->_migration->smallInteger()->defaultValue(UserModel::ACTIVE_WAIT),
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы пользователей
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'email'         => $this->_migration->getIndexTemplate($this->tableName, 'email', ['unique' => true]),
            'active'        => $this->_migration->getIndexTemplate($this->tableName, 'active'),
            'role'          => $this->_migration->getIndexTemplate($this->tableName, 'role'),
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}