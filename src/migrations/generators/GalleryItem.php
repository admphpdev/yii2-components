<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 11.12.15
 * Time: 13:57
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class GalleryItem extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы файлов
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'id_item'           => $this->_migration->integer(),
            'active'            => $this->_migration->integer()->defaultValue(\amd_php_dev\yii2_components\models\SmartRecord::ACTIVE_BLOCKED),
            'name'              => $this->_migration->string(255),
            'alt'               => $this->_migration->string(255),
            'content'           => $this->_migration->text(),
            'text'              => $this->_migration->text(),
            'priority'          => $this->_migration->integer(),
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы файлов
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'id_item'  => $this->_migration->getIndexTemplate($this->tableName, 'id_item'),
            'priority' => $this->_migration->getIndexTemplate($this->tableName, 'priority'),
            'active'   => $this->_migration->getIndexTemplate($this->tableName, 'active'),
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}