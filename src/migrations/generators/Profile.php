<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 14:47
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class Profile extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы профилей пользователей
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'            => $this->_migration->primaryKey(),
            'id_user'       => $this->_migration->integer(),
            'priority'       => $this->_migration->integer(),
            'active'        => $this->_migration->integer()->defaultValue(\amd_php_dev\yii2_components\models\SmartRecord::ACTIVE_BLOCKED),
            'fullname'      => $this->_migration->string(255),
            'phone_mobile'  => $this->_migration->string(255),
            'image_full'    => $this->_migration->string(255),
            'image_small'   => $this->_migration->string(255),
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы профилей пользователей
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'priority' => $this->_migration->getIndexTemplate($this->tableName, 'priority'),
            'id_user' => $this->_migration->getIndexTemplate($this->tableName, 'id_user', ['unique' => true])
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}