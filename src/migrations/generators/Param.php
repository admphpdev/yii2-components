<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 14:46
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class Param extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы параметров
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'        => $this->_migration->primaryKey(),
            'param'     => $this->_migration->string(128),
            'value'     => $this->_migration->text(),
            'default'   => $this->_migration->text(),
            'active'            => $this->_migration->integer()->defaultValue(0),
            'name'      => $this->_migration->string(255),
            'type'      => $this->_migration->string(128)->defaultValue('string'),
            'input_type'=> $this->_migration->integer(10)->defaultValue(0),
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы параметров
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'param' => $this->_migration->getIndexTemplate($this->tableName, 'param', ['unique' => true]),
            'active' => $this->_migration->getIndexTemplate($this->tableName, 'active'),
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }
}