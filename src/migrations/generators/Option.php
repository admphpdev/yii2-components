<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 11:43
 */

namespace amd_php_dev\yii2_components\migrations\generators;


class Option extends Generator
{
    /**
     * Возвращает шаблон колонок таблицы опций
     * @return array
     */
    public function getColumnsTemplate() {
        $columns = [
            'id'                => $this->_migration->primaryKey(),
            'priority'          => $this->_migration->smallInteger(),
            // Показ на странице
            'active'            => $this->_migration->boolean(),
            // Участвует в фильтре
            'in_filter'         => $this->_migration->boolean(),
            'required'          => $this->_migration->boolean(),
            'code'              => $this->_migration->string(255),
            'image'             => $this->_migration->string(255),
            'type'              => $this->_migration->string(255),
            'name'              => $this->_migration->string(255),
            'variants'          => $this->_migration->text(),
            'default'           => $this->_migration->text(),
            'description'       => $this->_migration->text(),
        ];

        return array_merge(parent::getColumnsTemplate(), $columns);
    }

    /**
     * Возвращает шаблон индексов таблицы опций
     * @return array
     */
    public function getIndexesTemplate() {
        $indexes = [
            'active'    => $this->_migration->getIndexTemplate($this->tableName, 'active'),
            'in_filter' => $this->_migration->getIndexTemplate($this->tableName, 'in_filter'),
            'code'      => $this->_migration->getIndexTemplate($this->tableName, 'code'),
            'priority'  => $this->_migration->getIndexTemplate($this->tableName, 'priority'),
        ];

        return array_merge(parent::getIndexesTemplate(), $indexes);
    }

}