<?php
/**
 * User: lagprophet
 * Date: 22.12.15
 * Time: 17:56
 */

namespace amd_php_dev\yii2_components\actions;


use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class GalleryAction extends \yii\base\Action
{

    /**
     * Класс модели галереи
     * @var string
     */
    public $modelClass;

    public $postData;

    /**
     * Название класса поведения галереи
     * @var string
     */
    public $galleryManagerName;

    public function runWithParams()
    {
        $this->postData = \Yii::$app->request->post();

        if ($this->postData['type'] == 'file') {
            $this->proccessFileItem();
        } elseif ($this->postData['type'] == 'image') {
            $this->proccessImageItem();
        } elseif ($this->postData['type'] == 'video') {
            $this->proccessVideoItem();
        }
    }

    public function proccessFileItem()
    {
        $postData = $this->postData;
        $model = new $this->modelClass;
        $galleryManager = $model->getBehavior($this->galleryManagerName);
        $galleryItemModel = $galleryManager->getNewItem();

        $file = \yii\web\UploadedFile::getInstancesByName('file')[0];
        $newName = $galleryItemModel->id . '.' . $file->getExtension();

        $web  = \Yii::getAlias('@web' . \Yii::$app->params['DATA_TEMP_DIR'] . $newName);
        $this->saveFile($file, \Yii::$app->params['DATA_TEMP_DIR'], $newName);

        $galleryItemModel->content = $web;

        $content = \amd_php_dev\yii2_components\widgets\form\gallery\GalleryItem::widget([
            'model' => $galleryItemModel,
            'type' => $postData['type']
        ]);

        echo $this->renderView($content);
    }

    public function proccessImageItem()
    {
        $postData = $this->postData;
        $model = new $this->modelClass;
        $galleryManager = $model->getBehavior($this->galleryManagerName);
        $galleryItemModel = $galleryManager->getNewItem();

        $file = \yii\web\UploadedFile::getInstancesByName('file')[0];
        $newName = $galleryItemModel->id . '.' . $file->getExtension();

        $web  = \Yii::getAlias('@web' . \Yii::$app->params['DATA_TEMP_DIR'] . $newName);
        $this->saveFile($file, \Yii::$app->params['DATA_TEMP_DIR'], $newName);

        $galleryItemModel->content = $web;

        $content = \amd_php_dev\yii2_components\widgets\form\gallery\GalleryItem::widget([
            'model' => $galleryItemModel,
            'type' => $postData['type']
        ]);

        echo $this->renderView($content);
    }

    public function proccessVideoItem()
    {
        $postData = $this->postData;
        $model = new $this->modelClass;
        $galleryManager = $model->getBehavior($this->galleryManagerName);
        $galleryItemModel = $galleryManager->getNewItem();

        $content = \amd_php_dev\yii2_components\widgets\form\gallery\GalleryItem::widget([
            'model' => $galleryItemModel,
            'type' => $postData['type']
        ]);

        echo $this->renderView($content);
    }

    public function renderView($content)
    {
        \Yii::setAlias('@galActView', __DIR__ . '/views');
        return $this->controller->renderAjax(
            '@galActView/gallery', ['content' => $content]
        );
    }

    protected function saveFile(UploadedFile $file, $dir, $name)
    {
        $dirPath = rtrim(\Yii::getAlias('@webroot' . $dir), '/');
        if (is_dir($dirPath) || FileHelper::createDirectory($dirPath, 0777)) {
            return $file->saveAs("{$dirPath}/{$name}");
        } else {
            return false;
        }
    }
}