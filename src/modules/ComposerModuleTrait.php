<?php
/**
 * Created by PhpStorm.
 * User: v-11
 * Date: 21.12.2016
 * Time: 12:58
 */

namespace amd_php_dev\yii2_components\modules;


trait ComposerModuleTrait
{
    public $_viewPath;

    public function getViewPath()
    {
        if ($this->_viewPath === null) {
            $this->_viewPath = 'composer/module/' . $this->getUniqueId() . DIRECTORY_SEPARATOR . 'views';
        }
        return $this->_viewPath;
    }
}