<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 04.04.16
 * Time: 12:00
 */

namespace amd_php_dev\yii2_components\modules;


class Admin extends Module
{
    public $layout      = '@app/views/layouts/admin';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}