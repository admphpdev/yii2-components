<?php
namespace amd_php_dev\yii2_components\modules;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 04.04.16
 * Time: 11:50
 */
class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    public $urlRules;
    public $urlRulesMerge = true;
    protected $_urlRules = [];

    public function init()
    {
        parent::init();

        /**
         * Для консоли контроллеры искать в папке commands
         */
        if (\Yii::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = preg_replace(
                '/^(.*\\)controllers$/',
                '$1commands',
                $this->controllerNamespace
            );
        }
    }
    /**
     * Возвращает массив с меню для виджета \amd_php_dev\yii2_components\widgets\Menu
     * @return array
     */
    public static function getMenuItems() {
        return [];
    }

    protected static $_classPathAlias;

    /**
     * Выполняется пери инициализации приложения
     * @param type $app Экзепляр приложения
     */
    public function bootstrap($app)
    {
        // Регистрируем правила url для модуля
        $this->_registerUrlRules($app);
        $this->setMenuItems();
    }

    /**
     * Возвращает массив правил для обработки url
     *
     * return [
     *     [
     *         'rules' => [...rules[]...],
     *         'append' => true,
     *     ],
     * ];
     *
     * @return array
     */
    public function getUrlRules()
    {
        $result = $this->_urlRules;

        if (!empty($this->urlRules) && is_array($this->urlRules)) {
            if ($this->urlRulesMerge) {
                $result = ArrayHelper::merge($result, $this->urlRules);;
            } else {
                $result = $this->urlRules;
            }
        }

        return $result;
    }

    /**
     * добавляет пункты в меню
     */
    public function setMenuItems() {

        \amd_php_dev\yii2_components\widgets\Menu::getInstance()->addItems(static::getMenuItems());

        foreach ($this->modules as $module) {
            \amd_php_dev\yii2_components\widgets\Menu::getInstance()->addItems(
                call_user_func_array([$module['class'], 'self::getMenuItems'], [])
            );
        }
    }

    /**
     * Регистрирует правила url для модуля
     * Правила берутся из функции getUrlRules
     * @param \yii\web\Application $app Экзепляр приложения
     */
    protected function _registerUrlRules($app) {
        $rules = $this->getUrlRules();
        if (!empty($rules)) {
            foreach ($rules as $rule) {
                if (!empty($rule))
                    $app->getUrlManager()->addRules($rule['rules'], ($rule['append'] === false) ? false : true);
            }
        }
    }
}