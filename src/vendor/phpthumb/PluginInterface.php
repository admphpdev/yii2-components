<?php

namespace amd_php_dev\yii2_components\vendor\phpthumb;

interface PluginInterface
{
    /**
     * @param  PHPThumb $phpthumb
     * @return PHPThumb
     */
    public function execute($phpthumb);
}
