<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 22.12.15
 * Time: 9:07
 */

namespace amd_php_dev\yii2_components\controllers;

/**
 * Class PublicController
 * Базовый класс контроллера публичной части сайта
 * @package amd_php_dev\yii2_components\controllers
 */
class PublicController extends Controller
{

}