<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 16.12.15
 * Time: 15:04
 */

namespace amd_php_dev\yii2_components\controllers;

/**
 * Class CrudController
 * Базовый класс CRUD контроллера
 * @package amd_php_dev\yii2_components\controllers
 */
abstract class CrudController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Создаёт новую модель
     * @param \amd_php_dev\yii2_components\models\SmartModel $model
     * @return bool результат сохранения модели
     */
    protected function createModel($model)
    {
        if ($model->load(\Yii::$app->request->post())) {
            return $model->save();
        }

        return false;
    }

    /**
     * Обновляет модель
     * @param \amd_php_dev\yii2_components\models\SmartModel $model
     * @return bool результат сохранения модели
     */
    protected function updateModel($model)
    {
        if ($model->load(\Yii::$app->request->post())) {
            return $model->save();
        }

        return false;
    }

    /**
     * Обновляет модель черех ajax, отправляет отчет пользователю и выключает перезапись связанных данных
     * @param \amd_php_dev\yii2_components\models\SmartModel $model
     * @return void
     */
    public function updateModelAjax($model)
    {
        if (property_exists($model, 'relationChange')) {
            $model->relationChange = false;
        }

        if ($this->updateModel($model)) {
            $modelName = (!empty($model->name)) ? $model->name : $model->primaryKey;
            \amd_php_dev\yii2_components\helpers\AjaxHelper::echoReport('success', ["Изменения в `{$modelName}` сохранены."]);
        } else {
            // Если данные пришли, но не валидны
            if (!empty($model->getErrors())) {
                \amd_php_dev\yii2_components\helpers\AjaxHelper::echoReport('error', $model->getErrors());
            } else {
                \amd_php_dev\yii2_components\helpers\AjaxHelper::echoReport('error', ['Что-то не так!']);
            }
        }
    }
}