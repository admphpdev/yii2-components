<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 22.05.2016
 * Time: 13:56
 */

namespace amd_php_dev\yii2_components\validators;


use yii\validators\Validator;

class AutoUniqueValidator extends Validator
{
    /**
     * @param \yii\db\ActiveRecord $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $query = $model->find()
            ->where("{$attribute} = :attribute", [':attribute' => $model->{$attribute}])
            ->select($attribute)
            ->asArray();

        if (!$model->isNewRecord) {
            $query->andWhere($model->primaryKey()[0] . ' != ' . (int) $model->primaryKey);
        }

        $similar = $query->one();

        if (!empty($similar)) {
            $this->setUniqueAttributeValue($model, $attribute);
        }
    }

    public function setUniqueAttributeValue($model, $attribute) {
        $query = $model->find()
            ->andFilterWhere(['like', $attribute, $model->{$attribute}])
            ->select($attribute)
            ->asArray();

        $similars = $query->all();

        $numbers = [];

        foreach ($similars as $similar) {
            $regexp = '^' . preg_quote($model->{$attribute}, '/') . '-(\d+)$';
            if (preg_match('/' . $regexp . '/', $similar[$attribute], $matches)) {
                $numbers[] = (int) $matches[1];
            }
        }

        if (is_array($numbers) && !empty($numbers)) {
            natsort($numbers);
            $model->{$attribute} .= '-' . (array_pop($numbers) + 1);
        } else {
            $model->{$attribute} .= '-1';
        }
    }
}