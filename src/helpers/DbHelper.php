<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 11.12.15
 * Time: 19:11
 */

namespace amd_php_dev\yii2_components\helpers;


class DbHelper
{

    /**
     * Преобразовывает массив, чтобы его можно было использовать в SQL запросе, не боясь того, что будет SQL-инъекция
     * @param array $params
     * @return string
     */
    public static function quoteArray($params)
    {
        $result = [];
        foreach ($params as $param) {
            if (is_numeric($param)) {
                $result[] = (int) $param;
            } else {
                $result[] = \Yii::$app->db->quoteValue($param);
            }
        }
        return implode(', ', $result);
    }

    /**
     * Применяет наборы функций-условий для объекта запроса
     * [
     *  'scope1',
     *  'scope2' => ['param1', 'param2']
     * ]
     * @param \yii\db\ActiveQuery $query
     * @param array $scopes
     * @return \yii\db\ActiveQuery
     */
    public static function scopeQuery(\yii\db\ActiveQuery $query, array $scopes)
    {
        if (!empty($scopes)) {
            foreach ($scopes as $key => $scope) {

                if (is_array($scope)) {
                    call_user_func_array([$query, $key], $scope);
                } else {
                    $query->{$scope}();
                }
            }
        }

        return $query;
    }

    public static function searchRemeber($formName, array $queryParams)
    {
        $sessionKey = 'searchRemember';
        $sessionModelKey = "{$sessionKey}.{$formName}";
        $session = \Yii::$app->session;
        $sessionRemember = [];
        if (!empty($session->get($sessionModelKey))) {
            $sessionRemember = $session->get($sessionModelKey);
        }

        $searchQuery = !empty($queryParams[$formName]) ? $queryParams[$formName] : [];

        if (!empty($searchQuery)) {
            $sessionRemember = $searchQuery;
        } elseif (empty($sessionRemember)) {
            $sessionRemember = [];
        }

        $sessionRemember = array_filter($sessionRemember, function($e) { return isset($e) ? true : false; });

        $session->remove($sessionModelKey);
        $session->set($sessionModelKey, $sessionRemember);

        return [$formName => $sessionRemember];
    }

}