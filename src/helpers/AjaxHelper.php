<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 03.12.15
 * Time: 18:56
 */

namespace amd_php_dev\yii2_components\helpers;


class AjaxHelper
{
    /**
     * Выводит отчет в формате json со статусом и сообщениями
     * @param $status
     * @param array $messages
     * @param bool $exit Завершить ли работу приложения после вывода отчета?
     */
    public static function echoReport($status, array $messages, $exit = true)
    {
        echo json_encode([
            'status' => $status,
            'messages' => $messages
        ]);
        if ($exit)
            \Yii::$app->end();
    }
}