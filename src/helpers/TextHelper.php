<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 05.04.16
 * Time: 14:26
 */

namespace amd_php_dev\yii2_components\helpers;


class TextHelper
{
    public static function formatPhone($string)
    {
        $result = str_replace('+7', '8', $string);
        $result = preg_replace('/[^\d]/', '', $result);
        return $result;
    }

    public static function transliterate($string)
    {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'i',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'kh',  'ц' => 'ts',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',    'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'Kh',  'Ц' => 'Ts',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',    'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );

        return strtr($string, $converter);
    }

    public static function str2url($str)
    {
        // переводим в транслит
        $str = self::transliterate($str);
        // в нижний регистр
        $str = strtolower($str);
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, "-");
        return $str;
    }

    public static function isUrl($string)
    {
        return !preg_match('/[^-a-z0-9_]/', $string);
    }

    public static function trimText($text, $length = 200, $stripTags = true, $postfix = '...')
    {
        if ($stripTags) {
            $text = strip_tags($text);
        }

        if (strlen($text) > $length) {
            $text = substr($text, 0, $length);
            if (($space = strrpos($text, ' ')) !== false) {
                $text = substr($text, 0, $space);
            }
            $text = rtrim($text, "!,.-");
            $text .= "…";
        }

        return  $text;
    }
}