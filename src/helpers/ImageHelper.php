<?php
/**
 * Created by PhpStorm.
 * User: v-11
 * Date: 20.01.2017
 * Time: 14:28
 */

namespace amd_php_dev\yii2_components\helpers;

use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\imagine\Image;

/**
 * Class ImageHelper
 * @package amd_php_dev\yii2_components\helpers
 *
 * -- Затребовать в composer.json yii\imagine\Image и Imagine\Image\Box
 */
class ImageHelper
{
    const MKDIR_MODE = 0755;

    /**
     * Обрежет изображение до максимальных размеров
     *
     * @param string $filePath Путь к файлу изображения
     * @param string $fileUrl Url файла изображения
     * @param integer $maxWidth Максимальная ширина итогового изображения
     * @param integer $maxHeight Максимальная высота итогового изображения
     * @return boolean false - если файл не удалось создать или object - в случае успеха
     */
    public static function resizeWithCache(
        $filePath,
        $fileUrl,
        $maxWidth,
        $maxHeight,
        $cacheTime = 3600,
        $forceUpdate = false
    ) {
        $update = $forceUpdate;
        $result = false;
        // Вычисляем новое имя файл и url
        $filePath = self::normalizePath($filePath);
        if (empty($filePath) || empty($fileUrl)) {
            return $result;
        }
        // Получаем информацию о файле
        $pathInfo = pathinfo($filePath);
        // Получить имя файла без расширения
        $cleanName = str_replace(".{$pathInfo['extension']}", '', $pathInfo['basename']);
        $newName = "{$cleanName}_{$maxWidth}_{$maxHeight}.{$pathInfo['extension']}";
        $newFilePath = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $newName;
        $newFileUrl = str_replace($pathInfo['basename'], $newName, $fileUrl);
        // Формируем результат
        $result = [
            'url' => $newFileUrl,
            'path' => $newFilePath
        ];
        // Нужно ли обновить файл по кешу
        if (
            !$update &&
            !$path = self::normalizePath($newFilePath) &&
                self::checkFileCache($path, $cacheTime)
        ) {
            $update = true;
        }
        // Обрезаем изображение если нужно
        if ($update) {
            // Получаем изображение
            if (!$image = self::getImage($filePath)) {
                return false;
            }
            //Устанавливаем размеры
            $box = new Box($maxWidth, $maxHeight);
            // Сохряняем с новым именем
            if (!$image->resize($box)->save($newFilePath)) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Вернёт Imagine объект изображения, если файл существует
     *
     * @param string $filePath полный путь или алиас к файлу
     * @return bool|\Imagine\Image\ImageInterface
     */
    protected static function getImage($filePath)
    {
        $path = \Yii::getAlias($filePath, false);
        if (!file_exists($path)) {
            return false;
        }
        return Image::getImagine()->open($path);
    }

    protected static function normalizePath($path)
    {
        $path = FileHelper::normalizePath(\Yii::getAlias($path, false));
        if (!file_exists($path)) {
            $path = false;
        }

        return $path;
    }

    protected static function checkFileCache($path, $time)
    {
        $filemtime = filemtime($path);
        return (!empty($filemtime) && (($filemtime + $time) < time()));
    }
}