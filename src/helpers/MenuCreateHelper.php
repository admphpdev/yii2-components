<?php
/**
 * User: lagprophet
 * Date: 15.12.15
 * Time: 18:39
 */

namespace amd_php_dev\yii2_components\helpers;

class MenuCreateHelper
{

    public static function processCategory($category)
    {
        return ['label' => $category->name];
    }

    /**
     * Создает массив для \yii\widgets\Menu
     *
     * Метод находит все дочерние категории и формирует массив.
     * Функция работает рекурсивно.
     *
     * @param \amd_php_dev\yii2_components\models\Page[] $categories
     * @return array
     */
    public static function menuFromCategoryWithChildren($categories, $modelClass)
    {
        $result = [];
        foreach ($categories as $category) {
            $item = self::processCategory($category);
            $children = $modelClass::find()->where(['id_parent' => $category->id])->all();
            $item += self::menuFromCategoryWithChildren($children);
            $result['items'] = $item;
        }
        return $result;
    }
}