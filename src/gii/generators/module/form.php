<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator \amd_php_dev\yii2_components\gii\generators\module\Generator */

?>
<div class="module-form">
<?php
    echo $form->field($generator, 'moduleClass');
    echo $form->field($generator, 'moduleBaseClass')->widget(\kartik\select2\Select2::className(), [
        'data' => $generator->getModuleBaseClasses(),
        'pluginOptions' => [
            'tags' => true,
        ],
    ]);
    echo $form->field($generator, 'moduleID');
?>
</div>
