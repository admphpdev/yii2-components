<?php
/**
 * This is the template for generating a module class file.
 */

/* @var $this yii\web\View */
/* @var $generator \amd_php_dev\yii2_components\gii\generators\module\Generator */

$className = $generator->moduleClass;
$pos = strrpos($className, '\\');
$ns = ltrim(substr($className, 0, $pos), '\\');
$className = substr($className, $pos + 1);

echo "<?php\n";
?>

namespace <?= $ns ?>;

/**
 * <?= $generator->moduleID ?> module definition class
 */
class <?= $className ?> extends <?= $generator->moduleBaseClass . PHP_EOL; ?>
{
    //public $layout      = '@app/views/layouts/default';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = '<?= $generator->getControllerNamespace() ?>';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        //$this->modules = [
        //
        //];

        // custom initialization code goes here
    }

    //public static function getMenuItems() {
    //    return [
    //        'section' => '<?= $generator->moduleID ?>',
    //        'items' => [
    //            [
    //                'label' => '<?= $generator->moduleID ?>',
    //                'items' => [
    //                    ['label' => 'label', 'url' => ['']],
    //                ]
    //            ]
    //        ],
    //    ];
    //}
}
