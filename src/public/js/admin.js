/**
 * Created by nofuture17 on 23.11.15.
 */
$(document).ready(function() {
    /**
     * Автоматическое включение нужного bootstrap таба
     */
    (function () {
        var hash = window.location.hash;
        if (hash) {
            if (hash.indexOf('tab') !== -1) {
                $('[href="' + hash + '"]').tab('show');
            }
        }
    })();

    /**
     * Вешаем обработчики для транслита
     */
    $('body').on('change', '[data-translit]', function (e){
        e.preventDefault();
        var item = $(this);
        if (item.val()) {
            var translited = window.lib.helpers.translit(item.val());
            item.val(translited);
        }
    });

    /**
     * Вешаем обработчики для копирования значения другого поля
     */
    $('[data-copy]').each(function(index, value){
        var item = $(value);
        var donorSelector = item.data('copy');

        if (item.data('model-pk')) {
            donorSelector = donorSelector + '[data-model-pk="' + item.data('model-pk') + '"]';
        }

        $('body').on('change', donorSelector, function (e){
            e.preventDefault();
            if (!item.val()) {
                item.val($(this).val());
            }
        });
    });

    /**
     * Обработчик для online-change полей
     */
    $(document).on('change', '[data-ajax-action="online-change"]',function (e) {
        e.preventDefault();
        var object = $(this);
        var data = {};

        data[object.attr('name')] = object.val();

        var url = decodeURIComponent(object.data('form-action'));

        if (url) {
            /**
             * Отправляем данные, выводим сообщения и убираем modal (если есть)
             */
            window.lib.helpers.sendData(url, data, "POST", function(res) {
                var messages = window.lib.helpers.getMessagesFromAjaxRes(res);
                window.lib.helpers.alertsRenderer(messages);
            });
        }
    });

    /**
     * Обработчик для вызова online-form
     */
    $(document).on('click', '[data-ajax-action="online-form"]', function(e) {
        e.preventDefault();
        var object = $(this);
        var sourceUrl = object.data('form-source');

        if (!sourceUrl) {
            sourceUrl = object.attr('href');
        }

        if (!sourceUrl) {
            return false;
        }

        $.ajax({
            method: "GET",
            url: sourceUrl,
            success: function (res) {
                if (!res) {
                    return false;
                }
                popup = new window.lib.objects.popup;
                popup.modalContent = res;
                popup.show();
            }
        });
    });

    /**
     * magnific
     */
    $('.thumbnail-single').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true
        }

    });

    /**
     * Плагин datatables
     */
    var jsonTables = $('[data-plugin="datatables"]');
    jsonTables.each(function () {
        var table = $(this).DataTable();
        table.on('click', '.deleteRow', function (e) {
            e.preventDefault();
            table.row($(this).parents('tr')).remove().draw();
        });

        table.on('click', '.addNewRow', function (e) {
            e.preventDefault();

            var inputs = $(this).parents('tr').find('input,select,textarea');
            var rowsCnt = table.rows()[0].length;

            if (!rowsCnt) {
                var newRowIndex = 1;
            } else {
                var newRowIndex = parseInt(table.column(0).data()[rowsCnt - 1]) + 1;
            }


            var newRow = [newRowIndex];
            var draw = false;

            inputs.each(function(){
                var input = $(this);
                var colContent = '';


                if (input.val() != '') {
                    draw = true;
                }

                colContent = window.lib.helpers.copyInput(input, true);

                var newInputName = colContent.input.attr('name').replace("[]", "[" + newRowIndex +"]");

                colContent.input.attr('name', newInputName);

                newRow.push(colContent.html());

                input.val('');
            });

            newRow.push('<button class="btn btn-danger deleteRow">Удалить</button>');

            if (draw) {
                table.row.add( newRow ).draw();
            }
        });
    });


    var uploadButtons = $('[data-plugin="ajax-upload"]');
    uploadButtons.each(function () {

        var button = $(this);

        var form = button.parents('form');

        var galleryPanel = button.parents('.gallery-panel');

        var options = {
            url: button.data('url'),
            maxFilesize: 2,
            //paramName: button.attr('name'),
            paramName: 'file',
            autoProcessQueue: true,
            uploadMultiple: true,
            parallelUploads: true,
            params: {
                _csrf: form.find('[name="_csrf"]').val(),
                action: 'add',
                type: button.data('type')
            },
            createImageThumbnails: false,
            previewTemplate: '<div></div>',
            clickable: ".fileinput-button"
        };

        var myDropzone = new Dropzone(button.parent().get(0), options);

        myDropzone.on("success", function(file, res) {
            galleryPanel.find('.gallery-items__container').append(res);
        });

        //button.dropzone(options);
    });

    var videoUploaders = $('[data-plugin="video-upload"]');
    videoUploaders.each(function () {
        var $this = $(this);
        var galleryPanel = $this.parents('.gallery-panel');
        $this.on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: $this.data('url'),
                method: 'POST',
                data: {
                    type: $this.data('type'),
                },
                success: function (data) {
                    galleryPanel.find('.gallery-items__container').append(data);
                }
            });
        });
    });

    /**
     * Удаление картинки (видео, файла) из галереи
     */
    var deleteButtons = $('[data-delete]');
    deleteButtons.each(function () {
        var $this = $(this);
        $this.on('click', function () {
            if ($this.data('delete'))
            {
                $this.parents('.gallery-item-form-block').remove();
            }
        });
    });
});