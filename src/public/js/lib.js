/**
 * Created by nofuture17 on 23.11.15.
 */
(function(){
    /**
     * Объект, который хранит модули
     * @type {{}}
     */
    window.lib = {
        helpers: {},
        objects: {}
    };

    /**
     * Транслитерализация строк
     * @param text
     * @returns {*|string}
     */
    window.lib.helpers.translit = function(text) {
        var space = '-';
        var text = text.toLowerCase();
        var transl = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'i', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
            'о': 'o', 'п': 'p', 'р': 'r','с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'kh',
            'ц': 'ts', 'ч': 'ch', 'ш': 'sh', 'щ': 'sch', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya',
            ' ': space, '_': space, '`': space, '~': space, '!': space, '@': space,
            '#': space, '$': space, '%': space, '^': space, '&': space, '*': space,
            '(': space, ')': space,'-': space, '\=': space, '+': space, '[': space,
            ']': space, '\\': space, '|': space, '/': space,'.': space, ',': space,
            '{': space, '}': space, '\'': space, '"': space, ';': space, ':': space,
            '?': space, '<': space, '>': space, '№':space
        }

        var result = '';
        var curent_sim = '';

        for(i=0; i < text.length; i++) {
            // Если символ найден в массиве то меняем его
            if(transl[text[i]] != undefined) {
                if(curent_sim != transl[text[i]] || curent_sim != space){
                    result += transl[text[i]];
                    curent_sim = transl[text[i]];
                }
            }
            // Если нет, то оставляем так как есть
            else {
                result += text[i];
                curent_sim = text[i];
            }
        }

        return window.lib.helpers.trim(result);
    }

    /**
     * Обрезка строки
     * @param s
     * @returns {string|XML}
     */
    window.lib.helpers.trim = function(s) {
        s = s.replace(/^-/, '');
        return s.replace(/-$/, '');
    }


    /**
     * Выводит сообщения
     * @param messages {status, message}
     */
    window.lib.helpers.alertsRenderer = function(messages)
    {

        if (!$('#alerts-container').length) {
            $('<div id="alerts-container"></div>').appendTo('body');
        }


        messages.forEach(function (item) {
            var alert = $('<div class="alert-' + item.status + ' alert fade in">' +
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + item.message
                + '</div>').appendTo($('#alerts-container'));

            setTimeout(function () {alert.alert('close');}, 3000);
        });
    }

    /**
     * Отправляет данные и выводи сообщения функцией:
     * window.lib.helpers.alertsRenderer
     * @param url
     * @param data
     * @param method
     * @param callback функция отрабатывает после получения результата. Может принимать res (результат)
     */
    window.lib.helpers.sendData = function(url, data, method, callback)
    {
        if (method === undefined) {
            method = "POST";
        }
        var response;
        $.ajax({
            url: url,
            method: method,
            data: data,
            dataType: 'JSON',
            success: function (res) {
                callback(res);
            },
        });
    }

    /**
     * Получает результат и разбирает его на сообщения для window.lib.helpers.alertsRenderer
     */
    window.lib.helpers.getMessagesFromAjaxRes = function(res) {
        var messages = [];
        for (var i = 0; i < res.messages.length; i++) {
            messages.push({'status': res.status, 'message': res.messages[i]});
        }
        return messages;
    }

    /**
     * Получает результат и разбирает его на сообщения для window.lib.helpers.alertsRenderer
     */
    window.lib.helpers.copyInput = function(original, parent) {

        if (parent) {
            var parentTag = original.parent().clone(true);
            var input = parentTag.find('[name="' + original.attr('name') + '"]');
        } else {
            var parentTag = '';
            var input = original.clone(true);
        }

        if (input.get(0).tagName == 'TEXTAREA') {
            input.append(original.prop('value'));
        } else {
            input.attr('value', original.val());
        }



        return {
            outer : parentTag,
            input : input,
            html : function() {
                if (this.outer.length) {
                    return $(this.outer).get(0).outerHTML;
                } else {
                    return $(this.input).get(0).outerHTML;
                }
            }
        };
    }

    window.lib.helpers.getQueryParamByName = function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    /**
     * Прототип всплывающего окна
     * @type {{modalContent: string, modalId: string, modalTitle: string, instance: undefined, template: popup.template, render: popup.render}}
     */
    var popup = {
        modalContent: '',
        modalId: '',
        modalTitle : '',
        instance : undefined,

        template : function (modalContent, modalId, modalTitle)
        {
            var unique = new Date().getTime() * Math.random();

            if (!modalId) {
                var modalId = 'myModal_' + unique;
            }

            if (!modalContent) {
                var modalContent = '';
            }

            var modalLabel = 'myModalLabel' + unique;
            if (modalTitle) {
                var modalTitleTemplate = '<h4 class="modal-title" id="' + modalLabel + '">' + modalTitle + '</h4>';
            } else {
                modalTitleTemplate = '';
            }


            return '' +
                '<div class="modal fade" id="' + modalId + '" tabindex="-1" role="dialog" aria-labelledby="' + modalLabel + '">' +
                    '<div class="modal-dialog" role="document">' +
                        '<div class="modal-content">' +
                            '<div class="modal-header">' +
                                '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                modalTitleTemplate +
                            '</div>' +
                            '<div class="modal-body">' +
                                modalContent +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
        },
        render : function () {
            this.instance = $(this.template(this.modalContent, this.modalId, this.modalTitle));
            return this.instance;
        },
        show: function() {
            this.render();
            $('body').append(this.instance);
            this.instance.modal('show');
        }
    };

    /**
     * всплывающее окно
     * @type {{__proto__: {modalContent: string, modalId: string, modalTitle: string, instance: undefined, template: popup.template, render: popup.render}}}
     */
    window.lib.objects.popup = function () {
        this.__proto__ = popup;
    }

})();