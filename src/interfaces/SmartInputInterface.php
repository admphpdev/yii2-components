<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 02.12.15
 * Time: 18:20
 */

namespace amd_php_dev\yii2_components\interfaces;


interface SmartInputInterface
{
    /**
     * @param string $attribute Название атрибута
     * @return int Тип поля ввода
     */
    public function getInputType($attribute);

    /**
     * @param string $attribute Название атрибута
     * @return mixed Данные для поля
     */
    public function getInputData($attribute);

    /**
     * @param string $attribute Название атрибута
     * @return array Опции для поля формы
     */
    public function getInputOptions($attribute);

    /**
     * Возвращает название формы
     * @return string
     */
    public function formName();
}