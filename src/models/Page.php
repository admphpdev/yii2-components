<?php

namespace amd_php_dev\yii2_components\models;

use Yii;
use \amd_php_dev\yii2_components\behaviors\ImageUploadBehavior;
use \amd_php_dev\yii2_components\widgets\form\SmartInput;
use \yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $created_at Дата создания
 * @property integer $updated_at Дата обновления
 * @property integer $priority Приоритет
 * @property integer $active Активность
 * @property integer $author Автор
 * @property string $name Имя h1
 * @property string $name_small Краткое имя
 * @property string $snipets Снипеты
 * @property string $links Дополнительные ссылки
 * @property string $url URL
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $text_small
 * @property string $text_full
 * @property string $image_small
 * @property string $image_full
 */
class Page extends SmartRecord
{
    const ACTIVE_ARCHIVE    = 4;
    const ACTIVE_SKETCH     = 5;

    const IMAGES_URL_ALIAS = '@web/data/images/all/';

    public function behaviors()
    {
        return [
            '\amd_php_dev\yii2_components\behaviors\AuthorBehavior',
            TimestampBehavior::className(),
            [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'image_small',
                'createThumbsOnSave' => false,
                'maxWidth' => !empty(\Yii::$app->params['IMAGE_THUMB.MAX_WIDTH']) ? \Yii::$app->params['IMAGE_THUMB.MAX_WIDTH'] : 800,
                'maxHeight' => !empty(\Yii::$app->params['IMAGE_THUMB.MAX_HEIGHT']) ? \Yii::$app->params['IMAGE_THUMB.MAX_HEIGHT'] : 600,
                'fileUrl' => \Yii::getAlias(static::IMAGES_URL_ALIAS, false),
            ],
            [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'image_full',
                'createThumbsOnSave' => false,
                'maxWidth' => !empty(\Yii::$app->params['IMAGE_FULL.MAX_WIDTH']) ? \Yii::$app->params['IMAGE_FULL.MAX_WIDTH'] : 800,
                'maxHeight' => !empty(\Yii::$app->params['IMAGE_FULL.MAX_HEIGHT']) ? \Yii::$app->params['IMAGE_FULL.MAX_HEIGHT'] : 600,
                'fileUrl' => \Yii::getAlias(static::IMAGES_URL_ALIAS, false),
            ],
            'addLinksManager' => [
                'class'        => \amd_php_dev\yii2_components\behaviors\CustomObjectsFieldBehavior::className(),
                'objectClass'  => \amd_php_dev\yii2_components\models\CustomJsonObject::className(),
                'objectsField' => 'links',
                'objectAttributes' => ['code' => 'Код ссылки'],
            ],
            'snipetsManager' => [
                'class'        => \amd_php_dev\yii2_components\behaviors\CustomObjectsFieldBehavior::className(),
                'objectClass'  => \amd_php_dev\yii2_components\models\CustomJsonObject::className(),
                'objectsField' => 'snipets',
                'objectAttributes' => ['code' => 'Код снипета'],
            ],
        ];
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'priority':
                $result = SmartInput::TYPE_NUMBER;
                break;
            case 'image_small':
            case 'image_full':
                $result = SmartInput::TYPE_IMAGE_SINGLE;
                break;
            case 'text_full':
                $result = SmartInput::TYPE_REDACTOR;
                break;
            case 'text_small':
            case 'meta_description':
                $result = SmartInput::TYPE_TEXTAREA;
                break;
            case 'links':
                $result = SmartInput::TYPE_SMARTTABLE;
                break;
            case 'snipets':
                $result = SmartInput::TYPE_SMARTTABLE;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'links' :
                $result = $this->getBehavior('addLinksManager');
                break;
            case 'snipets' :
                $result = $this->getBehavior('snipetsManager');
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'url' :
                $result = [
                    'htmlOptions' => [
                        'class' => 'form-control',
                        'data-translit' => ""
                    ]
                ];
                break;
            case 'meta_title' :
                $result = [
                    'htmlOptions' => [
                        'class' => 'form-control',
                        'data-copy' => "[name=\"{$this->formName()}[name]\"]"
                    ]
                ];
                break;
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }

    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        return Url::to(['/page/default/index', 'url' => $this->url]);
    }

    public static function getActiveArray()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::getActiveArray(),
            [
                static::ACTIVE_ARCHIVE    => 'В архиве',
                static::ACTIVE_SKETCH     => 'Черновик',
            ]
        );
    }

    /**
     * @inheritdoc
     */
//    public static function tableName()
//    {
//        return '{{%page}}';
//    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['created_at', 'updated_at', 'author'], 'integer'],
            [['text_small', 'text_full', 'links', 'snipets'], 'string'],
            [['name', 'name_small', 'url', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
//            [['url'], \amd_php_dev\yii2_components\validators\AutoUniqueValidator::className()],

            ['active', 'integer'],
            ['active', 'default', 'value' => static::ACTIVE_WAIT],
            ['active', 'in', 'range' => array_keys(static::getActiveArray())],

            ['priority', 'integer'],
            ['priority', 'default', 'value' => 100],

            [['image_small', 'image_full'], 'safe'],
            ['image_small', 'file', 'extensions' => 'jpeg, jpg, gif, png'],
            ['image_full', 'file', 'extensions' => 'jpeg, jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'created_at' => 'Создан',
                'updated_at' => 'Обновлён',
                'priority' => 'Приоритет',
                'author' => 'Автор',
                'name' => 'Название',
                'name_small' => 'Краткое название',
                'url' => 'Url',
                'meta_title' => 'Заголовок title',
                'meta_keywords' => 'Meta Keywords',
                'meta_description' => 'Meta Description',
                'text_small' => 'Анонс',
                'text_full' => 'Полный текст',
                'image_small' => 'Изображение мал.',
                'image_full' => 'Изображение',
                'links' => 'Дополнительные ссылки',
                'snipets' => 'Снипеты',
            ]
        );
    }

    /**
     * @inheritdoc
     * @return PageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PageQuery(get_called_class());
    }
}
