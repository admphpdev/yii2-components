<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 02.06.2016
 * Time: 14:38
 */

namespace amd_php_dev\yii2_components\models;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $role
 * @property string $name
 * @property string $parent
 * @property integer $priority
 * @property integer $active
 */
abstract class UserRole extends SmartRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['role', 'string'],
            ['role', 'required'],
            ['role', 'unique'],
            ['name', 'string'],
            ['name', 'required'],
            ['parent', 'string'],
        ];

        if ($this->role != \amd_php_dev\yii2_components\models\User::ROLE_USER) {
            $rules = \yii\helpers\ArrayHelper::merge(
                $rules,
                [
                    ['parent', 'exist', 'targetAttribute' => 'role'],
                    ['parent', 'default', 'value' => \amd_php_dev\yii2_components\models\User::ROLE_USER],
                ]
            );
        }

        return \yii\helpers\ArrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(),
            [
                'name'            => 'Имя привилегии',
                'role'            => 'Код привилегии',
                'parent'          => 'Родительская привилегия',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'parent' :
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REGULAR_SELECT;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'parent' :
                $result = [];
                $query = $this->getParentRelation()->clean()->defaultScope()->asArray();
                if (!$this->isNewRecord) {
                    $query->andwhere('id != ' . (int) $this->id);
                }
                $data = $query->all();
                foreach ($data as $item) {
                    $result[$item['role']] = "{$item['role']} - {$item['name']}";
                }
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->isNewRecord || (!empty($this->oldAttributes['role']) && $this->oldAttributes['role'] != $this->role)) {
            $this->onChangeRole();
        } elseif (!empty($this->oldAttributes['parent']) && $this->oldAttributes['parent'] != $this->parent) {
            $this->onChangeParent();
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $role = \Yii::$app->authManager->getRole($this->role);
        \Yii::$app->authManager->removeChildren($role);
        \Yii::$app->authManager->remove($role);

        /**
         * @var $users \amd_php_dev\yii2_components\models\User[]
         */
        $users = $this->getUserRelation()->all();
        foreach ($users as $user) {
            $user->role = \amd_php_dev\yii2_components\models\User::ROLE_USER;
            $user->assignRole();
            $user->save();
        }
    }

    protected function onChangeRole()
    {
        if (!$role = \Yii::$app->authManager->getRole($this->role)) {
            $role = \Yii::$app->authManager->createRole($this->role);
            $role->description = $this->name;
            \Yii::$app->authManager->add($role);
        }

        if (!empty($this->oldAttributes['role'])) {
            $oldRole = \Yii::$app->authManager->getRole($this->oldAttributes['role']);
            \Yii::$app->authManager->remove($oldRole);
        }

        $this->onChangeParent($role);
    }

    protected function onChangeParent($role = null)
    {
        if (empty($role)) {
            $role = \Yii::$app->authManager->getRole($this->role);
        }

        if (!empty($this->oldAttributes['parent'])) {
            $oldParent = \Yii::$app->authManager->getRole($this->oldAttributes['parent']);
            \Yii::$app->authManager->removeChild($role, $oldParent);
        }

        $parent = \Yii::$app->authManager->getRole($this->parent);
        \Yii::$app->authManager->addChild($role, $parent);
    }

    /**
     * @return SmartQuery|null
     */
    public function getParentRelation()
    {
        return $this->hasOne(static::className(), ['role' => 'parent']);
    }
    
    abstract public function getUserRelation();
}