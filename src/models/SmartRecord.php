<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 22.12.15
 * Time: 10:23
 */

namespace amd_php_dev\yii2_components\models;


class SmartRecord extends \yii\db\ActiveRecord implements \amd_php_dev\yii2_components\interfaces\SmartInputInterface
{
    const ACTIVE_BLOCKED    = 1;
    const ACTIVE_WAIT       = 2;
    const ACTIVE_ACTIVE     = 3;

    /**
     * Флаг обновления связанных данных.
     *
     * Если установлен, данные будут обновлены.
     * @var bool
     */
    public $relationChange = true;

    public function getActiveName()
    {
        return \yii\helpers\ArrayHelper::getValue(static::getActiveArray(), $this->active);
    }

    public static function getActiveArray()
    {
        return [
            static::ACTIVE_ACTIVE     => 'Активен',
            static::ACTIVE_WAIT       => 'Ожидает',
            static::ACTIVE_BLOCKED    => 'Заблокирован',
        ];
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'active':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REGULAR_SELECT;
                break;
            case 'priority':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_NUMBER;
                break;
            default:
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_TEXT;
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'active' :
                $result = static::getActiveArray();
                break;
        }

        return $result;
    }

    public function getInputOptions($attribute)
    {
        $result = [
            'htmlOptions' => ['class' => 'form-control']
        ];

        if (!$this->isNewRecord) {
            $result['htmlOptions'] = \yii\helpers\ArrayHelper::merge(
                $result['htmlOptions'],
                ['data-model-pk' => $this->id]
            );
        }

        switch ($attribute) {
            case 'active' :
                $result = \yii\helpers\ArrayHelper::merge(
                    $result,
                    [
                        'valueToClass' => [
                            static::ACTIVE_ACTIVE     => 'bg-success-imp',
                            static::ACTIVE_WAIT       => 'bg-warning-imp',
                            static::ACTIVE_BLOCKED    => 'bg-danger-imp',
                        ]
                    ]
                );
                break;
        }

        return $result;
    }

    public function rules()
    {
        return [
            ['active', 'integer'],
            ['active', 'default', 'value' => static::ACTIVE_BLOCKED],
            ['active', 'in', 'range' => array_keys(static::getActiveArray())],
            ['priority', 'integer'],
            ['priority', 'default', 'value' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Активность',
            'priority' => 'Приоритет',
        ];
    }
}