<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 22.12.15
 * Time: 10:23
 */

namespace amd_php_dev\yii2_components\models;


use amd_php_dev\yii2_components\interfaces\SmartInputInterface;
use yii\base\Model;

class SmartModel extends Model implements SmartInputInterface
{
    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_TEXT;
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        return $result;
    }

    public function getInputOptions($attribute)
    {
        $result = [
            'htmlOptions' => ['class' => 'form-control']
        ];

        return $result;
    }
}