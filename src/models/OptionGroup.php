<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.04.16
 * Time: 11:19
 */

namespace amd_php_dev\yii2_components\models;


class OptionGroup extends SmartRecord
{
    const IMAGES_URL_ALIAS = '@web/data/images/all/';
    
    const ATTR_CHILDREN = 'children';
    
    public $options = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => \amd_php_dev\yii2_components\behaviors\ImageUploadBehavior::className(),
                'attribute' => 'image',
                'createThumbsOnSave' => false,
                'maxWidth' => !empty(\Yii::$app->params['IMAGE_THUMB.MAX_WIDTH']) ? \Yii::$app->params['IMAGE_THUMB.MAX_WIDTH'] : 800,
                'maxHeight' => !empty(\Yii::$app->params['IMAGE_THUMB.MAX_HEIGHT']) ? \Yii::$app->params['IMAGE_THUMB.MAX_HEIGHT'] : 600,
                'fileUrl' => \Yii::getAlias(static::IMAGES_URL_ALIAS, false),
            ],
            'childrenManager' => [
                'class' => \amd_php_dev\yii2_components\behaviors\HasManyBehavior::className(),
                'setableAttribute' => static::ATTR_CHILDREN,
                'relationName' => 'childrenRelation',
                'createObjects' => false,
            ],
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public static function getActiveArray()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::getActiveArray(), [
        //
        //]);
        return parent::getActiveArray();
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'image':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_IMAGE_SINGLE;
                break;
            case 'id_parent':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_SELECT;
                break;
            case static::ATTR_CHILDREN:
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_CATEGORIES;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_parent':
                $data = $this->find()->where(['id_parent' => 0])->asArray()->all();
                $result = [0 => 'Без родителя'];
                foreach ($data as $item) {
                    $result[$item['id']] = $item['id'] . ' - ' . $item['name'];
                }
                break;
            case static::ATTR_CHILDREN:
                $data = $this->find()->asArray()->all();
                $result = [];
                foreach ($data as $item) {
                    $result[$item['id']] = $item['id'] . ' - ' . $item['name'];
                }
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            ['code', 'string', 'max' => 255],
            ['code', 'unique'],
            ['name', 'string', 'max' => 255],
            ['name', 'required'],
            ['id_parent', 'integer'],
            [static::ATTR_CHILDREN, 'safe'],
            ['image', 'safe'],
            ['image', 'file', 'extensions' => 'jpeg, jpg, gif, png'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'code' => 'Код круппы',
            'name' => 'Название',
            'image' => 'Картинка',
            'id_parent' => 'Родитель',
            static::ATTR_CHILDREN => 'Дочерние группы'
        ]);
    }

    public function getParentRelation()
    {
        return $this->hasOne(static::className(), ['id' => 'id_parent']);
    }

    public function getChildrenRelation()
    {
        return $this->hasMany(static::className(), ['id_parent' => 'id']);
    }
}