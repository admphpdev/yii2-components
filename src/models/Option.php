<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.04.16
 * Time: 11:15
 */

namespace amd_php_dev\yii2_components\models;


use amd_php_dev\yii2_components\behaviors\OptionBehavior;

class Option extends SmartRecord
{
    const REQUIRED_TRUE = 1;
    const REQUIRED_FALSE = 0;

    const IN_FILTER_TRUE = 1;
    const IN_FILTER_FALSE = 0;

    const TYPE_STRING = 'string';
    const TYPE_REDACTOR = 'redactor';
    const TYPE_NUMBER = 'number';
    const TYPE_VARIANT = 'variant';

    const IMAGES_URL_ALIAS = '@web/data/images/all/';

    public $value;

    /**
     * @var OptionBehavior
     */
    public $optionsManager;

    public function getOptionValue()
    {
        if ($this->isNewRecord) {
            return null;
        }
        
        $values = $this->optionsManager->getValue();
        $value = !empty($values[$this->id]) ? $values[$this->id] : null;

        if (is_array($value) && isset($value['value'])) {
            $value = $value['value'];
        }

        return $value;
    }

    public static function getRequiredArray()
    {
        return [
            static::REQUIRED_FALSE => 'Нет',
            static::REQUIRED_TRUE => 'Да',
        ];
    }

    public static function getInFilterArray()
    {
        return [
            static::IN_FILTER_TRUE => 'Да',
            static::IN_FILTER_FALSE => 'Нет',
        ];
    }

    public static function getTypesArray()
    {
        return [
            static::TYPE_STRING => 'Строка',
            static::TYPE_NUMBER => 'Число',
            static::TYPE_REDACTOR => 'Текстовый редактор',
            static::TYPE_VARIANT => 'Один из вариантов',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => \amd_php_dev\yii2_components\behaviors\ImageUploadBehavior::className(),
                'attribute' => 'image',
                'createThumbsOnSave' => false,
                'maxWidth' => !empty(\Yii::$app->params['IMAGE_THUMB.MAX_WIDTH']) ? \Yii::$app->params['IMAGE_THUMB.MAX_WIDTH'] : 800,
                'maxHeight' => !empty(\Yii::$app->params['IMAGE_THUMB.MAX_HEIGHT']) ? \Yii::$app->params['IMAGE_THUMB.MAX_HEIGHT'] : 600,
                'fileUrl' => \Yii::getAlias(static::IMAGES_URL_ALIAS, false),
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function getActiveArray()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::getActiveArray(), [
        //
        //]);
        return parent::getActiveArray();
    }

    public function getVariantsArray()
    {
        if (empty($this->variants)) {
            return [];
        }

        return explode('|', $this->variants);
    }

    public function getIsRequired()
    {
        return (bool) $this->required;
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'in_filter' :
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REGULAR_SELECT;
                break;
            case 'required' :
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REGULAR_SELECT;
                break;
            case 'variants' :
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_TEXTAREA;
                break;
            case 'default' :
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_TEXTAREA;
                break;
            case 'description' :
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_TEXTAREA;
                break;
            case 'type' :
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_SELECT;
                break;
            case 'image':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_IMAGE_SINGLE;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'in_filter' :
                $result = static::getInFilterArray();
                break;
            case 'required' :
                $result = static::getRequiredArray();
                break;
            case 'type' :
                $result = static::getTypesArray();
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            ['name', 'required'],
            [['in_filter', 'required'], 'integer'],
            [['variants', 'default', 'description'], 'string'],
            [['code', 'type', 'name'], 'string', 'max' => 255],
            [['in_filter'], 'default', 'value' => static::IN_FILTER_TRUE],
            [['required'], 'default', 'value' => static::REQUIRED_FALSE],
            [['type'], 'default', 'value' => static::TYPE_STRING],
            [['variants'], 'default', 'value' => ' '],
            [['code'], 'unique'],
            [['image'], 'safe'],
            ['image', 'file', 'extensions' => 'jpeg, jpg, gif, png'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'in_filter' => 'Отображать в фильтре',
            'required' => 'Обязательна для заполнения',
            'variants' => 'Варианты выбора через "|"',
            'type' => 'Тип значения',
            'code' => 'Код опции',
            'name' => 'Название',
            'default' => 'Значение по умолчанию',
            'description' => 'Описание',
            'image' => 'Картинка',
        ]);
    }

    /**
     * @inheritdoc
     * @return CatalogMachineryOptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OptionQuery(get_called_class());
    }
}