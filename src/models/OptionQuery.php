<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.04.16
 * Time: 11:15
 */

namespace amd_php_dev\yii2_components\models;


class OptionQuery extends SmartQuery
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        //return ArrayHelper::merge(parent::behaviors(), [
        //
        //]);
        return parent::behaviors();
    }

    /**
     * @inheritdoc
     * @return CatalogMachineryOption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CatalogMachineryOption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}