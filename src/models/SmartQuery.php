<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 22.12.15
 * Time: 10:24
 */

namespace amd_php_dev\yii2_components\models;


class SmartQuery extends \yii\db\ActiveQuery
{
    public $offCache = false;

    public function offCache($offCache = true) {
        $this->offCache = $offCache;
        return $this;
    }

    public function getModelTableName()
    {
        return call_user_func("$this->modelClass::tableName");
    }

    public function defaultScope() {
        return $this->active()->defaultSort();
    }

    public function defaultSort() {
        $this->orderBy(
            $this->getModelTableName() . '.`priority` DESC, ' .
            $this->getModelTableName() . '.`id` ASC');
        return $this;
    }

    public function byPk($id)
    {
        if (is_array($id)) {
            $ids = array_map(function($e) { return (int) $e; }, $id);
            $this->andWhere(['in', $this->getModelTableName() . '.`id`', $ids]);
        }else {
            $this->andWhere($this->getModelTableName() . '.`id` = :id', [':id' => $id]);
        }
        return $this;
    }

    public function active()
    {
        $this->andWhere(
            [
                $this->getModelTableName() . '.`active`' => \amd_php_dev\yii2_components\models\SmartRecord::ACTIVE_ACTIVE
            ]
        );
        return $this;
    }

    /**
     * Время жизни кеша по умолчанию
     * @var int
     */
    public static $cacheDuration = 3600;

    /**
     * Возвращает чистый объект запроса
     * @return \amd_php_dev\yii2_components\models\SmartQuery
     */
    public function clean()
    {
        $class = get_called_class();
        $query = new $class($this->modelClass);
        return $query;
    }

    /**
     * Зависимость кеша по умолчанию
     * @return \yii\caching\DbDependency|null
     */
    public static function getCacheDependency()
    {
        return null;
    }

    /**
     * @inheritdoc
     * @return Page[]|array|null
     */
    public function all($db = null)
    {
        if ($this->offCache) {
            return parent::all($db);
        }

        $db = \Yii::$app->db;
        return $db->cache(
            function($db){
                return parent::all($db);
            }, static::$cacheDuration,
            static::getCacheDependency()
        );
    }

    /**
     * @inheritdoc
     * @return Page|array|null
     */
    public function one($db = null)
    {
        if ($this->offCache) {
            return parent::one($db);
        }

        $db = \Yii::$app->db;
        return $db->cache(
            function($db){
                return parent::one($db);
            }, static::$cacheDuration,
            static::getCacheDependency()
        );
    }

    public function getFullTableName($tableName) {
        return \yii::$app->db . $this->getCleanTableName($tableName);
    }

    //{{%tableName}}
    public function getCleanTableName($tableName) {
        return trim($tableName, '{%}');
    }
}