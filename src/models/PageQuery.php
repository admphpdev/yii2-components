<?php

namespace amd_php_dev\yii2_components\models;

/**
 * This is the ActiveQuery class for [[Page]].
 *
 * @see Page
 */
class PageQuery extends SmartQuery
{
    public function url($url) {
        $this->andWhere($this->getModelTableName() . '.`url` = :url', [':url' => $url]);
        return $this;
    }
}