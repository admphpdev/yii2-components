<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 15:01
 */

namespace amd_php_dev\yii2_components\models;

use \amd_php_dev\yii2_components\models\SmartRecord;
use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $username
 * @property string $auth_key
 * @property string $email_confirm_token
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $login
 * @property string $role
 * @property integer $active
 */
abstract class User extends SmartRecord implements \yii\web\IdentityInterface
{

    // Числовые коды роли пользователя
    const ROLE_ADMIN        = 'admin';
    const ROLE_MODERATOR    = 'moderator';
    const ROLE_JOURNALIST   = 'journalist';
    const ROLE_USER         = 'user';

    public function getActiveName()
    {
        return ArrayHelper::getValue(self::getActiveArray(), $this->active);
    }

    public static function getActiveArray()
    {
        return ArrayHelper::merge(parent::getActiveArray(),
            [
                self::ACTIVE_WAIT => 'Не подтверждён',
            ]
        );
    }

    public function getRoleName()
    {
        return ArrayHelper::getValue(self::getRolesArray(), $this->role);
    }

    public static function getRolesArray()
    {
        return [
            self::ROLE_USER         => 'Польователь',
            self::ROLE_JOURNALIST   => 'Журналист',
            self::ROLE_MODERATOR    => 'Модератор',
            self::ROLE_ADMIN        => 'Администратор',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Правила для поля username
            ['username', 'required'],
            ['username', 'string', 'min' => 3, 'max' => 255],

            // Правила для поля email
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => self::className(), 'message' => 'Пользователь с таким email существует'],
            ['email', 'string', 'max' => 255],

            // Правила для поля active
            ['active', 'integer'],
            ['active', 'default', 'value' => self::ACTIVE_WAIT],
            ['active', 'in', 'range' => array_keys(self::getActiveArray())],

            // Правила для поля role
            ['role', 'string'],
            ['role', 'default', 'value' => self::ROLE_USER],
//            ['role', 'in', 'range' => array_keys(self::getRolesArray())],

            // Общие правила
            [['created_at', 'updated_at'], 'integer'],
            ['login', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'created_at'    => 'Создан',
            'updated_at'    => 'Обновлён',
            'username'      => 'Имя',
            'password_hash' => 'Пароль',
            'email'         => 'Email',
            'active'        => 'Активность',
            'role'          => 'Привилегии',
            'login'         => 'Псевдоним'
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    // Методы userIdentity
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'active' => self::ACTIVE_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->generateAuthKey();
            }
            return true;
        }
        return false;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'active' => self::ACTIVE_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        // Время жЫсьни ключа
        $expire = 3600;
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param string $email_confirm_token
     * @return static|null
     */
    public static function findByEmailConfirmToken($email_confirm_token)
    {
        return static::findOne(['email_confirm_token' => $email_confirm_token, 'active' => self::ACTIVE_WAIT]);
    }

    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }

    public function assignRole() {
        \Yii::$app->authManager->assign(\Yii::$app->authManager->getRole($this->role), $this->id);
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'role' :
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REGULAR_SELECT;
                break;
            case 'password_hash' :
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_PASSWORD;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'role' :
                $result = [];
                $data = $this->getRoleRelation()->clean()->defaultScope()->asArray()->all();
                foreach ($data as $item) {
                    $result[$item['role']] = "{$item['role']} - {$item['name']}";
                }
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }
    
    abstract public function getRoleRelation();

    abstract public function getProfileRelation();
}