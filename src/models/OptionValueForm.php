<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 28.04.16
 * Time: 7:02
 */

namespace amd_php_dev\yii2_components\models;


class OptionValueForm extends SmartModel
{
    /**
     * @var SmartRecord
     */
    public $model;
    /**
     * @var Option
     */
    public $optionModel;
    public $formAttribute = 'optionValues';
    public $value;

    public function setData(
        $model, $optionModel, $formAttribute, $value = null, $errors = null
    )
    {
        $this->model = $model;
        $this->optionModel = $optionModel;
        $this->formAttribute = $formAttribute;

        if (!empty($value)) {
            $this->value = $value;
        } elseif (!empty($optionModel->default)) {
            $this->value = $optionModel->default;
        }

        if (!empty($errors)) {
            foreach ($errors as $error) {
                $this->addError('value', $error);
            }
        }
    }

    public function formName()
    {
        $reflector = new \ReflectionClass($this->model);
        return $reflector->getShortName() . "[{$this->formAttribute}][{$this->optionModel->id}]";
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'value' :
                switch ($this->optionModel->type) {
                    case Option::TYPE_NUMBER :
                        $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_NUMBER;
                        break;
                    case Option::TYPE_VARIANT :
                        $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_SELECT;
                        break;
                    case Option::TYPE_STRING :
                        $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_TEXT;
                        break;
                    case Option::TYPE_REDACTOR :
                        $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REDACTOR;
                        break;
                    default :
                        $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_TEXT;
                }
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'value' :
                switch ($this->optionModel->type) {
                    case Option::TYPE_VARIANT :
                        $variants = $this->optionModel->getVariantsArray();
                        foreach ($variants as $variant) {
                            $result[$variant] = $variant;
                        }
                        break;
                }
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['value', 'safe']
        ];

        if (!empty($this->optionModel->default)) {
            $rules[] = ['value', 'default', 'value' => $this->optionModel->default];
        }

        if ($this->optionModel->isRequired) {
            $rules[] = ['value', 'required'];
        }

        if ($this->optionModel->type == \amd_php_dev\yii2_components\models\Option::TYPE_VARIANT) {
            $rules[] = ['value', 'in', 'range' => $this->optionModel->getVariantsArray()];
        }

        return \yii\helpers\ArrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'value' => $this->optionModel->name,
        ]);
    }
}