<?php

namespace amd_php_dev\yii2_components\models;

use amd_php_dev\yii2_components\widgets\form\SmartInput;

/**
 *
 * @property string $url
 * @property string $h1
 * @property string $metaDescription
 * @property string $text
 * @property string $metaTitle
 * @property integer $id
 * @property integer $active
 * @property integer $priority
 */
abstract class MetaData extends \amd_php_dev\yii2_components\models\SmartRecord
{
    /**
    * @inheritdoc
    */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'text' :
                $result = SmartInput::TYPE_REDACTOR;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'priority'], 'integer'],
            ['active', 'default', 'value' => static::ACTIVE_WAIT],
            [['url', 'h1', 'metaDescription'], 'string', 'max' => 255],
            [['metaTitle'], 'string', 'max' => 80],
            ['text', 'string'],
            [['url'], 'required']
        ];
    }
}
