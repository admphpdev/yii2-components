<?php

namespace amd_php_dev\yii2_components\models\comment;

use amd_php_dev\yii2_components\models\SmartRecord;
use Yii;
use \yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%catalog_comment}}".
 *
 * @property string $text
 * @property integer $id_item
 * @property integer $id_owner
 * @property integer $id_parent
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $id
 * @property integer $active
 * @property integer $priority
 */
abstract class Comment extends \amd_php_dev\yii2_components\models\SmartRecord
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            TimestampBehavior::className(),
        ]);
    }

    /**
     * @inheritdoc
     * @return CommentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }

    /**
     * @return CommentOwner the active query used by this AR class.
     */
    abstract public function getOwnerRelation();

    /**
     * @return CommentLikeQuery
     */
    abstract public function getLikesRelation();

    /**
     * @return SmartRecord
     */
    abstract public function getItemRelation();
}
