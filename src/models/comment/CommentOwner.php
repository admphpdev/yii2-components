<?php

namespace amd_php_dev\yii2_components\models\comment;

use Yii;

/**
 * This is the model class for table "{{%catalog_comment_owner}}".
 *
 * @property string $name
 * @property string $image
 * @property string $ip
 * @property string $email
 * @property integer $id
 * @property integer $active
 * @property integer $priority
 */
abstract class CommentOwner extends \amd_php_dev\yii2_components\models\SmartRecord
{
    const IMAGES_URL_ALIAS = '@web/data/images/all/';

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'image',
                'createThumbsOnSave' => false,
                'maxWidth' => !empty(\Yii::$app->params['IMAGE_FULL.MAX_WIDTH']) ? \Yii::$app->params['IMAGE_FULL.MAX_WIDTH'] : 800,
                'maxHeight' => !empty(\Yii::$app->params['IMAGE_FULL.MAX_HEIGHT']) ? \Yii::$app->params['IMAGE_FULL.MAX_HEIGHT'] : 600,
                'fileUrl' => \Yii::getAlias(static::IMAGES_URL_ALIAS, false),
            ],
        ]);
    }

    /**
     * @inheritdoc
     * @return CommentOwnerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentOwnerQuery(get_called_class());
    }

    /**
     * @return CommentQuery
     */
    abstract public function getCommentsRelation();

    /**
     * @return CommentLikeQuery
     */
    abstract public function getLikesRelation();
}
