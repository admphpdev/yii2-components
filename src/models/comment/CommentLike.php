<?php

namespace amd_php_dev\yii2_components\models\comment;

use Yii;

/**
 * This is the model class for table "{{%catalog_comment_like}}".
 *
 * @property integer $id_owner
 * @property integer $id_comment
 * @property integer $id
 * @property integer $active
 * @property integer $priority
 */
abstract class CommentLike extends \amd_php_dev\yii2_components\models\SmartRecord
{
    /**
     * @inheritdoc
     * @return CommentLikeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentLikeQuery(get_called_class());
    }

    /**
     * @return CommentOwner the active query used by this AR class.
     */
    abstract public function getOwnerRelation();

    /**
     * @return Comment the active query used by this AR class.
     */
    abstract public function getCommnetRelation();
}
