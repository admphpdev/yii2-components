<?php

namespace amd_php_dev\yii2_components\models\gallery;

/**
 * This is the ActiveQuery class for [[GalleryItem]].
 *
 * @see GalleryItem
 */
class GalleryItemQuery extends \amd_php_dev\yii2_components\models\SmartQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return GalleryItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return GalleryItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}