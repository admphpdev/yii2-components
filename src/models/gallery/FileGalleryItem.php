<?php
/**
 * User: lagprophet
 * Date: 22.12.15
 * Time: 14:56
 */

namespace amd_php_dev\yii2_components\models\gallery;


class FileGalleryItem extends GalleryItem
{
    public function rules()
    {
        return array_merge(
            parent::rules(),
            []
        );
    }

    public static function getGalleryType()
    {
        return \amd_php_dev\yii2_components\behaviors\GalleryManager::TYPE_FILE;
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function getInputOptions($attribute)
    {
        return parent::getInputOptions($attribute);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'content' => 'Файл',
            ]
        );
    }

    /**
     * @inheritdoc
     * @return FileGalleryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FileGalleryItemQuery(get_called_class());
    }

}