<?php
/**
 * User: lagprophet
 * Date: 22.12.15
 * Time: 15:06
 */

namespace amd_php_dev\yii2_components\models\gallery;


class VideoGalleryItem extends GalleryItem
{
    public function rules()
    {
        return array_merge(
            parent::rules(),
            []
        );
    }

    // Методы SmartInputInterface
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    public static function getGalleryType()
    {
        return \amd_php_dev\yii2_components\behaviors\GalleryManager::TYPE_VIDEO;
    }

    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function getInputOptions($attribute)
    {
        return parent::getInputOptions($attribute);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'content' => 'Код видео',
            ]
        );
    }

    /**
     * @inheritdoc
     * @return VideoGalleryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VideoGalleryItemQuery(get_called_class());
    }

}