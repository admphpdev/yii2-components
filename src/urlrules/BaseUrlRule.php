<?php
/**
 * Created by PhpStorm.
 * User: v-11
 * Date: 22.12.2016
 * Time: 14:13
 */

namespace amd_php_dev\yii2_components\urlrules;

use yii\web\UrlRuleInterface;
use yii\base\Object;

class BaseUrlRule extends Object implements UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {
        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        return false;  // this rule does not apply
    }
}